#!/usr/local/bin/python

"""Sky probe Filter wheel system
"""
import sys

class __FilterWheel:
	"""
	"""

	numFilters = 5
	sequence = []
	defNames = ['g','r','i','z','y']
	filterNames = ['']*numFilters
	nameLookUp = {}

	def __init__(self):
		"""Instantiate the filter wheel.
		"""
		# inititialize the filter position names
		self.setFilterNames(self.defNames)


	def renameFilter(self, n, newName):
		"""Assign a name to a filter position.
		"""
		if n in range(self.numFilters): 
			if self.nameLookUp.has_key(newName):
				if n != self.nameLookUp[newName]:
					#raise FilterName, "filter name "+newName+" is not unique"
					pass
				del self.nameLookUp[newName]
	
			self.filterNames[n] = newName 
			self.nameLookUp[newName] = n
		else:
			raise FilterName, "filter position "+str(n)+"not in range"


	def setFilterNames(self, newNames):
		"""Set the names in one go.
		"""
		if len(newNames) != self.numFilters: 
			raise FilterName, "number of filters don't match"
			return 

		self.nameLookUp = {}
		for i in range(self.numFilters):
			self.filterNames[i] = newNames[i]
			self.nameLookUp[newNames[i]] = i


	def checkState(self):
		"""Raise an error if two filters have the same name.
		"""
		if len(self.nameLookUp) != self.numFilters:
			raise FilterName, "one or more filter names are not unique"



	def getFilterNames(self):
		"""Return the list of filter names.
		"""
		return self.filterNames
		
###################################################
# Errors 
###################################################
	
class Error(Exception):
        """
        """
        pass

class FilterName(Error):
        """
        """
        def __init__(self, mesg):
                """
                """
                print >>sys.stderr, mesg


# create a singleton instance
__FW = __FilterWheel()

###################################################
# Public access
###################################################
# Name change commands
def renameFilter(n, newName):
	"""Assign a new name to position n
	"""
	__FW.renameFilter(n, newName)

def setFilterNames(newNames):
	"""Set the names list	
	"""
	__FW.setFilterNames(newNames)

def checkFilterState():
	"""Raise a FilterName exception if there is some
	   inconsistency with the names.   eg if two positions
           are assigned the same name.
	"""
	__FW.checkState()

def getFilterNames():
	"""Return a list of filter names.
	"""
	return __FW.getFilterNames()


#############################################
# Positioning commands
def getStatus():
	"""Return the status of the wheel
	"""
	return READY

def getPosition():
	"""Return the current position.
	"""
	pass

def setPosition():
	"""Move the filter wheel to position.
	"""
	pass

def isMoving():
	"""Check if the filter wheel is moving.
	"""
	pass

def waitUntilDone():
	"""Wait until the wheel has stopped.
	"""
	while(isMoving): time.sleep(0.1)

def setDirection(direction):
	"""Set the default direction the wheel goes.
	"""
	pass

def moveNext():
	"""Move to the next filter defined by the direction
	   of rotation.
	"""
	pass

###################################################
# self test 
###################################################

def test():
	"""
	"""
	print "testing filterwheel"	
	try:
		setFilterNames(["1","2","3"])
	except FilterName:
		print "good"
	try:
		renameFilter(1,'a')
	except FilterName:
		print "bad"

	try:
		renameFilter(2,'a')
	except FilterName:
		print "bad"
	print getFilterNames()

	try:
		checkFilterState()
	except FilterName:
		print "good" 


if __name__ == "__main__":
	"""
	"""
	test()	
