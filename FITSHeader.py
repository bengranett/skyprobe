""" A class to manipulate fits header keywords
    It uses dictionaries to save the data.
"""
import sys

#define TBIT          1  /* codes for FITS table data types */
#define TBYTE        11
#define TLOGICAL     14
#define TSTRING      16
#define TUSHORT      20
#define TSHORT       21
#define TUINT        30
#define TINT         31
#define TULONG       40
#define TLONG        41
#define TFLOAT       42
#define TDOUBLE      82
#define TCOMPLEX     83
#define TDBLCOMPLEX 163

class FITSHead:
    """ 
        The comments and values are stored in dictionaries
        indexed by the fits label.
    """
    comments = {}
    values = {}


    def setKey(self, keyword, value, comment=None):
        """ Add or change a keyword.
            If comment is set to None, it will not be changed.
        """
        value = self.valueToString(value)

        if not self.values.has_key(keyword) and not self.comments.has_key(keyword):
            if comment==None: comment=""
        
        if value != None:
            self.values[keyword] = value;

        if comment != None:
            self.comments[keyword] = comment;

    def delKey(self, keyword, comment=False):
        """ Delete a single keyword, optionally
            forget the comment too.
        """
        
        if self.values.has_key(keyword):
            del self.values[keyword]
            
        if comment:
            if self.comments.has_key(keyword):
                del self.comments[keyword]
        return
                        

    def delAll(self, comment=False):
        """ Delete all the keywords
            Optionally forget the comments too
        """
        self.values={}
        if comment: self.comments={}

    def keys(self):
        """ Access the list of keys
        """
        return self.values.keys()

    def getValue(self, key):
        """ Access a key/value pair
            Return an empty string if it doesn't exist
        """
        if not self.values.has_key(key): return ""
        return self.values[key]

    def getComment(self, key):
        """ Access a key/comment pair
        """
        if not self.comments.has_key(key): return ""   
        return self.comments[key]


    def loadFile(self, fileName):
        """ Load an ascii file with the fits header keywords
            It reads it in the standard fits format, but with one
            keyword entry per line.
            It will not understand if the same keyword shows up
            multiple times in the file, like HISTORY or COMMENT keywords.
            
            Format is:
            #comment line, ignored
            KEYWORD1=DEFAULT VALUE       / COMMENT
            KEYWORD2=DEFAULT VALUE       / COMMENT
            ...
        """
        self.fileName = fileName
        i = 0
        
        try:
            fd = file(fileName)
        except IOError:
            print >>sys.stderr, "FITSHeader.py: Failed to open file",fileName
            return -1
        
        for line in fd:
            i+=1
            
            line=line.strip();
            if line[0] == "#": continue
            if line[0] == "": continue
            if line.startswith("END"): continue

            # parse the fits header
            # one entry per line
            # first field has a = after
            n = line.find("=")
            if n < 0:
                self.parseError(i)
                return -1
            
            field1 = line[0:n]
            rest = line[n+1:]
            key = field1.rstrip()

            if key == "":
                self.parseError(i)
                return -1

            n = rest.find("/")
            if n < 0:
                self.parseError(i)
                return -1
            
            field2 = rest[0:n]
            comment = rest[n+1:]
            comment = comment.strip()
            
            value = field2.strip()   # strip white space
            value = value.strip("'") # remove quotes if present
            value = value.strip()    # strip more white space

            # Save the fields in the dictionaries
            self.values[key] = value
            self.comments[key] = comment

        return 0

    def parseError(self, l):
        """ Flash an error message
        """
        print "FITSHeader.py: "+self.fileName+": could not parse line",l," aborted"

    def dumpHdr(self, fd=sys.stdout):
        """ write the header out to a file descriptor, defaults stdout
        """
        fd.write(self.__str__())

    def __str__(self):
        """ Convert to string for fun
        """
        if len(self.values) == 0: return "header is empty!\n"
        
        out = ""
        for key in self.values.keys():
            out+=key.rjust(8)+"= "+self.values[key].rjust(20)+"  / "+self.comments[key]+"\n"

        return out

def test():
    hdr = FITSHead()
    r = hdr.loadFile("FITSHeader.ascii")
    hdr.setKey("CCDTEMP",str(-12.00))
    hdr.setKey("new key","hi")
    hdr.delKey("FILTER")
    hdr.setKey("FILTER","R")
    hdr.dumpHdr()
    hdr.delAll()
    hdr.dumpHdr()
    hdr.delKey("i don't exist")

    r = hdr.loadFile("notexistant")
if __name__=="__main__":
    test();
    
