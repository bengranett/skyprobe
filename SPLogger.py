""" Set up a single connection to the log server.
"""

import Log

SPLog = Log.Log("ImSkyProbe")


def entry(mesg, level = 1):
    """
    """
    SPLog.entry(mesg, level = level)


def error(mesg, level = 1):
    """
    """
    SPLog.error(mesg, level = level)


if __name__=="__main__":
	error("testing the error log")
	entry("testing the entry log")
