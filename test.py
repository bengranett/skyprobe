import sys,traceback

class myException(Exception):
    def __init__(self, string):
        self.mesg = string
    def __str__(self):
        return self.mesg


class BadResponse(Exception):
    """The reply is not in the form that we want."""
    def __init__(self, cmd, ans, attempts):
        self.message = "Could not get valid response from motor.  \nCmd: %s\nans: %s\nattempts: %i\n"%(cmd, ans, attempts)
    def __str__(self):
        return self.message

try:
    #raise myException("hello")
    raise BadResponse("cmd","ans",4)
except:
    print "got exception"
    #print traceback.print_exc()
    info = sys.exc_info()
    print info
    print info[0], info[1]
    #print traceback.format_exc()
    #print traceback.format_exception_only(sys.last_type(), sys.last_value)


print "hi"
