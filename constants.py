""" definitions of useful contstants, classes and errors

I try to confine OtisCORBA definitions to this file.  Other files which need to
access an IDL defined type or CORBA error should import this and call the
copy.

"""


import OtisCORBA
from OtisCORBA import SkyProbe as OtisTypes


#
# interesting numbers
#
nan = float('nan')       # general purpose not-a-number
null = 'NULL'            # Used for NaN's sent to mysql
                         # It seems that mysql spits back NaN

#
# status codes
#
HOME_ME  = -2
ERROR    = -1
IDLE     =  0
READY    =  0

ACTIVE   =  1
CHANGING =  1
BUSY     =  1

EXPOSING =  2
READING  =  3

HOMING   =  4



#
# CCD cooler status codes
# These are the codes returned by the Apogee driver
#
CCDCoolerOff        = 0
CCDCoolerChanging   = 1
CCDCoolerCorrecting = 2
CCDCoolerRampingUp  = 3
CCDCoolerAtAmbient  = 4
CCDCoolerAtMax      = 5
CCDCoolerAtMin      = 6 
CCDCoolerAtSetPoint = 7

#
# Corba constants
#

# Readout modes
TWELVEBITFAST = OtisCORBA.SkyProbe.FAST12
TWELVEBITSLOW = OtisCORBA.SkyProbe.SLOW12
SIXTEENBITSLOW = OtisCORBA.SkyProbe.SLOW16

# exposure target types
DOME = OtisCORBA.DOME

#
# IDL type definition wrappers
#
Exposure = OtisTypes.Exposure


#
# CORBA error wrappers
#
CORBA_BadCommandException = OtisCORBA.BadCommandException
