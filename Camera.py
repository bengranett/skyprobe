"""
The real camera class.
"""

import threading, time
import Log

import pycamera
import SkyProbeDB
from SkyProbeConf import config
import constants as const


class _Camera:
    """
    """
    globalLock = threading.Lock()

    # status info
    status = {'lastExp': -1}
              

    def __init__(self):
        """
        """
        self.camera = pycamera.pyCamera()
        
            
    def lock(self):
        r = self.globalLock.acquire(False)
        if r:
            Log.entry("set camera lock")
        return r

    def unlock(self):
        Log.entry("reset camera lock")
        self.globalLock.release()

    
    def setReadOutMode(self, mode):
        """Currently this just allows toggling between 12 and 16 bit modes.
        It is recommended that you stick with 16 bit mode which reads out in 10 seconds.
        """
        self.camera.setDepth(mode)
        Log.entry("CCD Readout mode set to "+str(mode))
    
    def isExposing(self):
        """Return true if exposing now"""
        if self.camera.status['status'] == const.EXPOSING:
            return True
        return False
    
    def getStatus(self):
        """Get camera status, returns a dictionary, good luck figuring out the keys hehe
        """
        #elapsed = 0.
        #if self.isExposing():
        #    elapsed = time.time() - self.camera.status['startTime']
        #    print "elapsed",elapsed
            
        stat = self.camera.getStatus()
        #stat['Elapsed'] = elapsed
                 
        return stat


    def waitWhileExposing(self):
        """"""
        try:
            self.camera.waitWhileExposing(self.status['lastExp'])
        except:
            raise

 
    def waitWhileReading(self, timeout):
        """Wait until camera is idle."""
        try:
            self.camera.waitWhileReading(timeout)
        except:
            raise

    
    def expose(self, texp, fileName = None, header = None, block = False):
        """Start an exposure.
        """
        type = config['CCDExposureType']['TEST']
        if header:
            try:
                type = header['IMGTYPE']
            except:
                pass
        
        Log.entry("Starting exposure: "+str(texp))
        self.status['lastExp'] = texp
        
        if type == config['CCDExposureType']['BIAS']:
            self.camera.bias(fileName=fileName, info=header, block=block)
        elif type == config['CCDExposureType']['DARK']:
            self.camera.dark(texp, fileName=fileName, info=header, block=block)
        elif type == config['CCDExposureType']['LIGHT']:
            self.camera.snap(texp, fileName=fileName, info=header, block=block)
        else:
            self.camera.expose(texp, fileName=fileName, type=type, info=header, block=block)



    def setCCDCooler(self, status):
        """
        Set the state of the CCD cooler.  status is boolean on (True) or off (False).
        """
        self.camera.setCooler(status)

    def setCCDThermostat(self, celsius):
        """
        Set the temperature goal for the CCD
        """
        try:
            self.camera.setTemp(celsius)
        except:
            raise const.CORBA_BadCommandException

    def abort(self):
        """Abort an exposure.  Not implemented.
        """
        pass

    def read(self):
        """Read out the camera.  Not implemented.
        """
        pass

    def shutdown(self):
        """Shutdown the camera"""
        if self.camera:
            self.camera.shutdown()


    #class coolerParams:
    #    coolerOn
    #    goal
        

    #def configureCooler(self, coolerParams):
    #    """
    #    """

#------------------------------------ fake a singleton class
_inst = None
def Camera():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _Camera()
    return _inst
#------------------------------------------------------------

def test():
    cam = Camera()
    cam.expose(0, "test.fits", header={"FOCUS":100,"COMMENT":"hehe"})
    cam.shutdown()

if __name__=="__main__":
    test()
