from Fnorb.orb import CORBA
import OtisCORBA

orb = CORBA.ORB_init([], CORBA.ORB_ID)
#f=open("/home/skyprobe/SkyProbe.ior","r")
#ior=f.readline()
#f.close()

ior=getConfigIOR()

server = orb.string_to_object(ior)





def getConfigIOR():
    """
    Return the IOR string of the Config server.
    """
    import socket
    iorString = ""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect(("flaxen",1050))
        iorString = sock.recv(1024)
        sock.close()
    except socket.gaierror:
        print >>sys.stderr, "Cannot connect to server",self.configHost, self.configPort
    except socket.error:
        print >>sys.stderr, "Connection refused at",self.configHost, self.configPort
        
    return iorString
