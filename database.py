""" database wrapper
"""
import time
import MySQLdb
import Log 

class db:
    """Wrap mysql commands with easy to call routines.
    """
    connected = False
    lastReconnect = 0.0
        
    def __init__(self, host=None, user=None, passwd=None, db=None):
        """Initialize the database
        """
        self.host = host
        self.user = user
        self.passwd = passwd
        self.db = db

        self.connect()

    def connect(self):
        """Connect to a databse
        """
        try:
            self.db = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd, db=self.db)
            self.cursor=self.db.cursor()
            self.connected = True
        except:
            Log.error("Can't connect to mysql database "+self.host)
            self.connected = False
        return self.connected

    def reconnect(self, waitTime=60.):
        """Try to reconnect, but give up if called for frequently than the waitTime.
        """
        if not self.lastReconnect:
            self.lastReconnect = time.time()

        if time.time()-self.lastReconnect > waitTime:
                self.lastReconnect = time.time()
                return self.connect()
        return False
            

    def insert(self, table, labels, values):
        """Insert some data
        """
        if not self.connected:
            if not self.reconnect(): return

        # test if the values were entered as a single tuple
        # or a list of tuples
        # we need them formatted as a list, even if theres only one set
        if type(values) == type((1,)):
            values = [values]

        
        ########################################################
        # mysql can't handle NaN's, but it can handle NULLs
        # The following lines look for nans and replace them with NULLs
        newValues = []
        for tup in values:
            newTup = []
            for v in tup:
                if str(v) == 'nan' or v == None:
                    v = "NULL"
                newTup.append(v)
            newValues.append(tuple(newTup))
        values = newValues
        ########################################################
        

        
        cmd = "INSERT INTO "+table+" ("
        n = len(labels)
        for i in range(n-1):
            cmd += labels[i]+", "   
        cmd += labels[n-1]+") VALUES ("+"%s, "*(n-1)+"%s)"
        #print cmd

        # test if the values were entered as a single tuple
        # or a list of tuples
        if type(values) == type((1,)):
            values = [values]

        #print values
        try:
            self.cursor.executemany(cmd, values)    
        except:
            Log.error("mysql insert failed "+cmd)
            #print values, labels
            #raise

    def select(self, table, labels):
        """
        labels is a list of tuples
        labels = [(label, operator, value), (label, op, value)]
        value and operator strings can be left out
        """
        if not self.connected:
            if not self.reconnect(): return []

        
        labString = ""
        rangeString = ""


        for i in range(len(labels)):
            if not type(labels[i])==type((None,)):
                labels[i] = (labels[i], None, None)
            l = len(labels[i])
            if l == 1: labels[i] = (labels[i], None, None)

            #print labels[i]
            
            labString += labels[i][0]+", "
            if labels[i][1]:
                rangeString += "%s %s '%s' AND"% labels[i]

    
        rangeString = rangeString[:-4]  # remove the last AND
        labString = labString[:-2]
        #print rangeString

        if len(rangeString)>0: rangeString = " WHERE "+rangeString
        
        cmd = "SELECT "+labString+" FROM "+table+rangeString
        print cmd
        nLines = self.cursor.execute(cmd)
        out = self.cursor.fetchall()
        return out

    def exists(self, table, label, value):
	"""Check if the label exists in the table with given value.
	Returns true or false
	"""
	result = self.select(table, [(label, "=", value)])
	return len(result) > 0


    def getTable(self, table):
        """
        """
        cmd = "describe "+table
        nLines = self.cursor.execute(cmd)
        out = self.cursor.fetchall()
        return out



def test():
    d = db(host="",user="",passwd="",db="")

    print d.getTable("sky_probe_hk")

    
    labels = labels=["time",("temperature1","<",29),("temperature1",">",28)]
    
    print d.select("sky_probe_hk",labels)



if __name__=="__main__":
    test()
