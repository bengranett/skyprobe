#!/usr/local/bin/python

"""
Python interface to run a pontech stepper control board.
Uses pySerial to access the serial port.

June 1 2005 Ben Granett
"""

import sys
import time
import serial

class Motor:
    """
    Control your pontech stepper motor board (over a serial line.)
    """
    # constants
    FULLSTEP = 1
    HALFSTEP = 2

    FULLSTEPSIZE = 1.8
    HALFSTEPSIZE = 0.9
    
    POWERON  = 1
    POWEROFF = 0

    TIMEOUT_ERROR = 1

    # globals
    dev = "/dev/ttyUSB2"
    serial = None

    # settings
    powerMode = None
    stepMode = None
    stepSize = None
    
    def __init__(self, id):
        """
        Initialize a new motor instance.
        Opens a serial connection with pySerial module
        (do multiple serial instances interfere?)
        """
        # Set the motor id integer >0.  0 will
        # command all motors connected to the pontech boards
        # at once... probably not what you want.
        self.id = id
        self.boardStr = "BD"+self.intStr(self.id)

    def __open(self):
        """ make sure timeout is set or else we may hang.
        baudrate and settings are all default.
        """
        try:
            self.serial = serial.Serial(self.dev, timeout=1)
        except serial.serialutil.SerialException:
            print "could not open port!"
            print "you probably don'thave permission."



    def open(self, dev="/dev/ttyUSB1"):
        """
        """
        self.dev = dev
        try:
            self.__open()
        except:
            # if an error occured, just raise it again
            raise
        
        ###########################################
        # Set up the default settings
        ###########################################
        
        # set full or half stepping
        self.stepMode = self.HALFSTEP
        self.setStepMode(self.stepMode)

        # set power mode
        self.powerMode = self.POWEROFF
        self.setPowerMode(self.powerMode)

        # home the position to 0
        self.pos = 0
        #self.home(self.pos)

        # set the step delay
        self.stepDelay = 2000
        self.setDelay(self.stepDelay)

        # set the acceleration
        self.stepAccel = 0
        self.stepMin = 0
        self.setAccel(self.stepAccel, self.stepMin)

    def intStr(self, n):
        """
        Convert a number to a string, ensuring that it is an integer.
        """
        return str(int(n))

    def pause(self, x=5):
        """
        Snooze for 3 thousandths of a second.  Give this command to
        allow the control board time to process a request.
        """
        time.sleep(x/1000.)

        
    def close(self):
        """Close connection to motor.
        First power it off.
        """
        self.setPowerMode(self.POWEROFF)
        self.serial.close()

    def __write(self, string):
        """
        """
        try:
            self.serial.write(string)
            return
        except:
            # just try to open the port again
            pass
        
        self.pause(3)

        try:
            self.__open()
            self.serial.write(string)
            return
        except:
            # reraise the exception
            raise


    def __read(self):
        """
        """
        print "checking reply"
        try:
            ans = self.serial.readline();
            return ans
        except:
            pass
        
        self.pause(3)

        try:
            self.__open()
            self.serial.readline()
            return
        except:
            # reraise the exception
            raise

       
    def sendCmd(self, cmd, reply=0, intType = 0, trial = 0):
        """
        Send a command to the pontech board.
        if reply == 1, read the reply string
        terminated by a newline character.
        """
        cmd = cmd.rstrip() + "\r"

        if len(cmd) > 20:
            print >>sys.stderr, cmd,"\n longer than 20 characters\n"
        print "sending:",cmd

        try:
            self.__write(cmd)
        except:
            raise
        
        self.pause(3)
        
        if not reply: return

        try:
            ans = self.__read()
        except:
            raise # lets nurture this exception

        # Make sure that if we wanted an int,
        # we got something that looks like an int.
        if intType:
            try:
                int(ans)
                return ans
            except ValueError:
                trial += 1
                if trial > 10:
                    raise BadResponse(cmd,ans, trial)
                ans = self.sendCmd(cmd, reply = 1, intType = 1, trial = trial)
               
       
    
    def setBoardID(self, newID):
        """
        Change the board ID
        """
        if self.id == -1:
            self.id = newID
            self.boardStr = "BD"+self.intStr(self.id)
            return
        if self.id >= 0 and newID >= 0:
            cmd = self.boardStr+"WE0 "+self.intStr(newID)
            self.sendCmd(cmd)
            self.id = newID
            self.boardStr = "BD"+self.intStr(newID)
        return
    
    def getBoardID(self):
        """
        """
        return self.id

    def waitWhileMoving(self):
        """
        Returns when the motor has stopped moving
        """
        while self.isMoving(): pass

    def isMoving(self):
        """
        return dest pos - current pos
        returns 0 when not moving
        """
        cmd = self.boardStr+"RT"
        ans = self.sendCmd(cmd, reply=True, intType=True)
        
        return int(ans)

    def setStepMode(self, mode=None):
        """
        """
        if mode == self.FULLSTEP or mode == None:
            self.stepMode = self.FULLSTEP
            self.stepSize = self.FULLSTEPSIZE
            string = "SF"
        if mode == self.HALFSTEP:
            self.stepMode = self.HALFSTEP
            self.stepSize = self.FULLSTEPSIZE
            string = "SH"
        cmd = self.boardStr+string
        
        self.sendCmd(cmd)

    def getStepMode(self):
        """
        """
        return self.stepMode

    def setPowerMode(self, mode=None):
        """
        """
        if mode == self.POWEROFF or mode == None:
            self.powerMode = self.POWEROFF
            string = "SO"
        if mode == self.POWERON:
            self.powerMode = self.POWERON
            string = "SP"
        cmd = self.boardStr+string

        self.sendCmd(cmd)

    def getPowerMode(self):
        """
        """
        return self.powerMode

    def setDelay(self, n = 2000):
        """
        delay given in usec
        """
        self.stepDelay = n
        sdStr = "SD"+self.intStr(n)
        cmd = self.boardStr+sdStr
        self.sendCmd(cmd)

    def getDelay(self):
        """
        """
        cmd=self.boardStr+"RSD"
        answer = self.sendCmd(cmd, reply=1)
        self.stepDelay = int(answer)
        return self.stepDelay

    def setFreq(self, f = 1):
        """
        freq given in kHz
        """
        sd = int(1000/f/1.6)
        self.stepDelay = sd
        sdStr = self.intStr(sd)
        cmd = self.boardStr+sdStr
        self.sendCmd(cmd)
        
    def setAccel(self, sa, sm):
        """
        """
        self.stepAccel = sa
        self.stepMin = sm
        saStr = "SA"+self.intStr(sa)
        smStr = "SM"+self.intStr(sm)
        cmd = self.boardStr+saStr+smStr
        self.sendCmd(cmd)

    def getAccel(self):
        """
        """
        cmd=self.boardStr+"RSA"
        answer = self.sendCmd(cmd, reply=1)
        self.stepAccel = int(answer)
        
        cmd=self.boardStr+"RSM"
        answer = self.sendCmd(cmd, reply=1)
        self.stepMin = int(answer)
        
        return self.stepAccel, self.stepMin
    
    def setPos(self, n):
        """
        """
        self.pos = n
        cmd=self.boardStr+"TC5 "+"MI"+self.intStr(n)
        self.sendCmd(cmd)

    def getPos(self):
        """
        """
        cmd=self.boardStr+"RC"
        answer = self.sendCmd(cmd, reply=1, intType=True)
        self.pos = int(answer)
        return self.pos

    def setRelPos(self, inc):
        """
        """
        self.pos+=inc
        cmd=self.boardStr+"TC5 "+"II"+self.intStr(inc)
        self.sendCmd(cmd)

    def home(self, n=0):
        """
        """
        cmd=self.boardStr+"TC5 H-"
        self.sendCmd(cmd)
        self.waitWhileMoving()
        cmd=self.boardStr+"HM"+self.intStr(n)
        self.sendCmd(cmd)

    def stop(self):
        """
        """
        cmd=self.boardStr+"HI"
        self.sendCmd(cmd)
        
    def stopSlowly(self):
        """
        """
        cmd = self.boardStr+"H0"
        self.sendCmd(cmd)

    def readVoltage(self, pin):
        """
        """
        if pin==1 or pin==2:
            cmd = self.boardStr+"AD"+self.intStr(pin)
            answer = self.sendCmd(cmd, reply = 1)
            v = float(answer)/255 * 5.0
            return v
        return -1

    def readPins(self):
        """
        """
        cmd = self.boardStr+"RP"
        answer = self.sendCmd(cmd, reply = 1)
        word = answer.rstrip()
        bits = [word[i] for i in range(len(word))]
        return bits


    def cmd(self,cmd):
        """
        """
        print "<<",
        print self.sendCmd(cmd, reply = 1)

    def dump(self):
        """
        """
        print "----------------"
        print "motor ID ",self.id
        print "is it moving?",self.isMoving()
        print "step mode  = ",self.getStepMode()
        print "step power = ",self.getPowerMode()
        print "step pos   = ",self.getPos()
        print "step delay = ",self.getDelay()
        print "step SA,SM = ",self.getAccel()
        print "input pins = ",self.readPins()
        print "analog vol = ",self.readVoltage(1), self.readVoltage(2)
        

class Error(Exception):
    """
    """
    pass

class BadResponse(Error):
    """
    """
    def __init__(self,cmd, ans,attempts):
        """
        """
        self.attempts = attempts
        print >>sys.stderr, "Could not get valid response from motor"
        print >>sys.stderr, "cmd:",cmd
        print >>sys.stderr, "ans:",ans
        print >>sys.stderr, "attempts:",str(attempts)
        

def test():
    print "motor.py  ...running test"
   
    m1 = Motor(id=1)
    m1.open()
         
    m1.setAccel(100,3000)
    m1.setFreq(1000)
    #m1.dump()


    
    import random
    i = -1
    while True:
        i=-1*i
        #r = random.uniform(0,1)*i
        m1.setRelPos(400*i)
        m1.waitWhileMoving()
        time.sleep(1)
    

    #cmd = " "
    #while cmd[0] != ".":
    #    print ">>",
    #    cmd = sys.stdin.readline()
    #    m1.cmd(cmd)

    #count = 0
    #n = 0
    #for i in range(10):
    #    n+=1
    #    a = m1.sendCmd("RC", 1)
    #    #if a[0] != "0": count+=1
    #    time.sleep(1)

    m1.close()

    
    #print count
    #print n
    
if __name__=='__main__':
    test()
    
