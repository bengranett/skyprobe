#!/usr/local/bin/python
"""
SkyProbe server

Ben Granett
"""

#######################################################
# store filenames and such in SkyProbeConf
#######################################################
from SkyProbeConf import *

import sys, os, signal, fcntl, time
import ORBit,CORBA
import Otis
import Log

ORBit.load_file(IDL_FILE)
import OtisCORBA__POA
import OtisCORBA

verb = True

class Date:
    """
    an instance in time
    """
    # the number of seconds since 1970-01-01T00:00:00
    # excluding leap seconds.
    seconds = None
    
    # the number of nanoseconds since the last
    # integral second
    nanosec = None
    
    # true if the current time occurs during a leap second
    leapsec = None


class TrackPoint:
    """
    Specifies the path the telescope follows across the sky.
    interpolation should be done using a cubic spline.
    """
    time = Date()
    az = None
    alt = None
    rotator = None
    
class Star:
    """
    Specifies the elements in the star catalog
    """
    name = ""
    ra = None
    dec = None
    mag = None
    
    segment = ""
    x = None
    y = None



class SkyProbe(OtisCORBA__POA.SkyProbe):
    """
    The SkyProbe server
    """

    ALIVE = 1
    READY = 0
    ERROR = -1
    
    myName = MYNAME
    
    def __init__(self):
        """
        initialize server instance
        """
        
        
    
    ###############################
    # Generic server routines
    ###############################
    def getName(self):
        """
        return the server name
        """
        if verb: log.entry("Received getName command", 2)
        return self.myName

    def isAlive(self):
        """
        Am I alive?
        """
        if verb: log.entry("Received isAlive command", 2)
        return self.ALIVE
        
    def shutdown(self):
        """
        shutdown the server
        """
        if verb: log.entry("Received shutdown command", 2)
        global orb
        # shutdown(1) does not work...?
        # 0 return immediately
        # 1 block until processing is complete, then return
        orb.shutdown(0)
        
        
    ###############################
    # SkyProbe specific routines
    ###############################
    def getStatus(self):
        """
        Is the SkyProbe up and ready to go?  Return status code
        """
        if verb: log.entry("Received getStatus command", 2)
        return self.READY

    def go(self, exposure, id, ra, dec, track, starList):
        """
        Call when telescope is in position and SkyProbe should start work.
        """
        if verb: log.entry("Received go command", 2)
        pass

    def abort(self):
        """
        Interrupt the current sequence.
        """
        if verb: log.entry("Received abort command", 2)
        pass

    def powerDown(self):
        """
        SkyProbe should shutdown cleanly and power down.  Call in case of
        unexpected loss of power, or for routine shutdown.
        """
        if verb: log.entry("Received powerDown command", 2)
        self.shutdown()


def handleSIGINT(sigNum, stackFrame):
    """
    Catch an interrupt call and quit nicely
    """
    print >>sys.stderr, "received signal", sigNum, "shutting down"
    servant.shutdown()





def createDaemon():
    """
    Detach a process from the controlling terminal and run it in the
    background as a daemon.  Adapted from
    http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/278731
    """
    print "i'm turning into a daemon"
    
    try:
        pid = os.fork()
    except OSError, e:
        return((e.errno, e.strerror))
      
    # kill my parent
    if (pid > 0): os._exit(0)
    
    # call setsid() to become the session leader
    os.setsid()

    # ignore SIGHUPs.  The grandchild will receive
    # one when its parent dies.
    signal.signal(signal.SIGHUP, signal.SIG_IGN)
    
    try:
        pid = os.fork()        # Fork a grandchild
    except OSError, e:
        return((e.errno, e.strerror))
    
    if (pid > 0): os._exit(0)  # kill the first child

    # change to our home directory
    os.chdir(homeDir)
    
    # let me go crazy with permissions
    os.umask(0)

    
  
    # Now redirect std output  to log files
    # this is a bit tricky...
    #
    print "look in the log files for stdout and stderr"
    print " stdout:",stdoutFile
    print " stderr:",stderrFile

    
    # first we close ALL possible file descriptors
    # these go from 1 to SC_OPEN_MAX
    try:
        maxfd = os.sysconf("SC_OPEN_MAX")
    except (AttributeError, ValueError):
        maxfd = 256       # default maximum
    
    for fd in range(0, maxfd):
        try:
            os.close(fd)
        except OSError:   # ERROR (ignore)
            pass
    # reopen stdin,stdout and stderr but redirect them

    # Redirect the standard file descriptors to a log file
    fd0 = os.open("/dev/null", os.O_RDONLY)                       # stdin  (0)
    if fd0 != 0:
        os.close(fd0)
        return -1
    
    fd1 = os.open(stdoutFile, os.O_CREAT|os.O_APPEND|os.O_WRONLY) # stdout (1)
    if fd1 != 1:
        os.close(fd0)
        return -2
    
    fd2 = os.open(stderrFile, os.O_CREAT|os.O_APPEND|os.O_WRONLY) # stderr (2)
    if fd2 != 2:
        os.close(fd0)
        os.close(fd1)
        return -3
    

    # We should have a living daemon now!
    return 0


def printUsage(argv):
    """
    print usage statement
    """
    print "Usage:"
    print argv[0],"[-d]"
    print "########## Options ##############"
    print "--help  print this message"
    print "-d      turn into a daemon process"
    
#################################################
# parse command line
#################################################

daemonize = 0
restart = 0

for i in range(1, len(sys.argv)):
    opt = sys.argv[i].lstrip("-")
    if opt[0] == "h":
        printUsage(sys.argv)
        sys.exit(0)
        continue
    if opt == "d" or opt == "daemon":
        daemonize = 1
        continue
    if opt == "r" or opt == "restart":
        restart = 1
        continue
    # catch unrecognized options
    print "unrecognized option:",sys.argv[i]
    print ""
    printUsage(sys.argv)
    sys.exit(1)

##################################################
# See if I can restart
##################################################
if restart:
    print "restarting..."
    try:
        lockFile = file(lockFileName, 'r')
        pid = int(lockFile.read().strip())
        lockFile.close()
    except IOError:
        print >>stderr, "could not get pid of current server from",lockFileName
        sys.exit(1)

    print "gonna kill", pid
    try:
        while 1:
            os.kill(pid, signal.SIGTERM)
            time.sleep(1)
    except OSError, err:
        err = str(err)
        if err.find("No such process") > 0:
            pass
        else:
            print str(err)
            sys.exit(1)

    print "all set"
    print "here we go"
    # not sure if this is good, but
    # call myself from the shell...
    os.system(startCmd)
    sys.exit(0)
    
##################################################
# turn into a daemon
##################################################
if daemonize:
    res = createDaemon()
    if res != 0:
        print "could not tear myself off with a fork"
        sys.exit(1)


##################################################
# use a lock file to ensure we are the only
# skyprobe running
##################################################

# create the lock file if it doesn't exist
if not os.path.exists(lockFileName):
    lockFile = open(lockFileName, "w")
    lockFile.close()

lockFile = open(lockFileName, "r+")
# see if we can lock it, if not, it is already locked
# so quit
try:
    fcntl.flock(lockFile.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
except IOError:
    print sys.argv[0],"already running with pid",lockFile.readline()
    lockFile.close()
    os._exit(0)

# print our PID to the file
lockFile.write(str(os.getpid()))
lockFile.flush()


# Initialize the log
log = Log.Log(MYNAME, verb=verb)

#################################################
# First check if we are running.
# Now this is not really necessary if we use
# a lock file.
#################################################
#running = 0
#try:
#    sp = Otis.getServer(MYNAME, OtisCORBA.SkyProbe)
#    print "SkyProbe already running"
#    print "try quitting it first"
#    running = 1
#except:
#    pass0
#
#if running: sys.exit(1)

# Initialize the orb
orb = CORBA.ORB_init(sys.argv, "")

# Instantiate the server
servant = SkyProbe()
objref = servant._this()

 
# Get the IOR string
iorString = orb.object_to_string(objref)

# register with the name server
Otis.registerIOR(servant.getName(),iorString)


poa = orb.resolve_initial_references("RootPOA")
poa._get_the_POAManager().activate()

print "SkyProbe up"
log.entry("SkyProbe server 's up!", 3)

orb.run()

# remove entry from name server
Otis.removeIOR(servant.getName())


print "SkyProbe down"
log.entry("SkyProbe server has shutdown.", 3)


# unlock the lockfile
lockFile.close()
