"""Central location to maintain the database connection"""



from SkyProbeConf import config
import database




_inst = None
def ispdb():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = database.db(host=config['MYSQLHost'],      \
			    user=config['MYSQLUser'],     \
			    passwd=config['MYSQLPassword'], \
			    db=config['MYSQLDB'])
    return _inst

