#!/usr/local/bin/python

"""Sky Probe focus interface.

Notes for Ben:
abort focus should not run in a thread.  clean up that code

fixed focus status
"""
import sys, time
import threader, threading
import Log
import constants as const
from SkyProbeConf import config

if config['SkyProbeSim']:
    import FocusMotorSim as FocusMotor
else:
    import FocusMotor

class _Focus:
    """
    """

    status = {'status': const.HOME_ME,
              'position': -1,
              'focusHomed': False
              }
    
        
    # Create a lock for the focus-- this is set internally.
    intLock = threading.Lock()
    globalLock = threading.Lock()

    #abortEvent = threading.event()
    
    class setFocusCmd(threader.Command):
        """ A command class for threader to queue up.
        Set the focus position.
        """
        def __init__(self, parent, n, monitor):
            self.n = n
            self.parent = parent
            self.monitor = monitor
            
        def execute(self):
            try:
                self.parent._setFocus(self.n, self.monitor)
            except:
                self.error("Exception caught while setting focus.")
        
    class homeFocusCmd(threader.Command):
        """ A command class for threader to queue up.
            Zero the focus ring.
        """
        def __init__(self, parent, monitor):
            self.parent = parent
            self.monitor = monitor
        def execute(self):
            try:
                self.parent._homeFocus(self.monitor)
            except:
                self.error("Exception caught while zeroing focus")

    class updateStatusCmd(threader.Command):
        """ A thread to update the position status every so often.
        I've learned that we can't poll on every getstatus request when
        a bazilion clients are connected.
        """
        def __init__(self, parent, waitTime):
            self.parent = parent
            self.waitTime = waitTime
        def execute(self):
            try:
                self.parent._updateStatusLoop(self.waitTime)
            except:
                self.error("Exception caught in focus position polling loop")

    class abortFocusCmd(threader.Command):
        """ A command class for threader to queue up.
        Zero the focus ring.
        """
        def __init__(self, parent):
            self.parent = parent
                    
        def execute(self):
            try:
                self.parent._abortFocus()
            except:
                self.error()
    
    def __init__(self):
        """
        """
        self.status['status'] = const.IDLE
        self.abortEvent = threading.Event()

        # start the thread pool
        self.FocusThreadPool = threader.threadPool(5)

        try:
            self.motor = FocusMotor.Motor()
        except:
            raise

        # start motor polling thread
        cmd = self.updateStatusCmd(self, config['FocusPollTime'])
        self.FocusThreadPool.do(cmd, block=False)
       
    def lock(self):
        r = self.globalLock.acquire(False)
        if r:
            Log.entry("set focus lock")
        return r

    def unlock(self):
        Log.entry("reset focus lock")
        self.globalLock.release()

    def shutdown(self):
        """
        """
        self.abortEvent.set()
        self.motor.close()
        self.FocusThreadPool.shutdown()

    def _setFocus(self, n, monitor):
        """
        """        
        # First get the lock on the focus motor
        if not self.intLock.acquire(False):
            if monitor: monitor.setError("Could not acquire focus lock.")
            return

        # A negative position means the focus should not be changed.
        if n < 0: 
            self.intLock.release()
            if monitor: monitor.setDone()
            return

        # We are ready to move, set the status.
        self.status['status'] = const.CHANGING

        try:
            self.motor.setPos(n)
        except FocusMotor.TimeOutError, error:
            Log.error(error.message)
        except FocusMotor.HitLimitError, error:
            Log.error(error.message)
        except FocusMotor.PositionError, error:
            Log.error(error.message)

        self.updatePosition()
        Log.entry("Focus position set to: "+str(self.status['position']))

        self.status['status'] = const.IDLE
        #self.focusHomed = False
        self.intLock.release()

        #print "  locks released"
        if monitor: monitor.setDone()
        #print "  monitor set"


    def _homeFocus(self, monitor):
        """
        """
        self.status['status'] = const.HOMING
        self.intLock.acquire()

        try:
            self.motor.home(0)
        except FocusMotor.TimeOutError, error:
            Log.error(error.message)
        except FocusMotor.HitLimitError, error:
            Log.error(error.message)
        except FocusMotor.PositionError, error:
            Log.error(error.message)

            
        self.updatePosition()
        
        Log.entry("Focus homed.")
        self.status['focusHomed'] = True
        self.status['status'] = const.IDLE
        self.intLock.release()
        
        if monitor: monitor.setDone()


    def _abortFocus(self):
        """ stop any movement.  Set the error flag maybe.
        """
        self.motor.stop()
        Log.entry("Focus move abortted!")
        

    def setFocus(self, n, monitor = None, block = True):
        """ Set the focus to n.  This command will be handled by a thread.
        """
        if not self.status['focusHomed']:
            Log.entry("Homing the focus")
            self.homeFocus()
        
        cmd = self.setFocusCmd(self,n, monitor)
        self.FocusThreadPool.do(cmd, block)
        return cmd
    
    def homeFocus(self, monitor = None, block = True):
        """ Home the focus.  This will be performed by a thread.
        """
        cmd = self.homeFocusCmd(self, monitor)
        self.FocusThreadPool.do(cmd, block)
        return cmd
            
    def abortFocus(self):
        """
        """
      
        #self.FocusThreadPool.do(self.abortFocusCmd(self))
        self._abortFocus()
        return

    def updatePosition(self):
        """ Update the status position."""
        self.status['position'] = self.motor.getPos()

    def _updateStatusLoop(self, waitTime):
        """ Poll the motor forever or until the abort event is raised. """
        while not self.abortEvent.isSet():
            self.updatePosition()
            time.sleep(waitTime)
            
            
    def getStatus(self):
        """ return the busy status
        """
        return [self.status['status'], self.status['position'], self.status['focusHomed']]

    def getFocus(self):
        """
        """
        return self.position
            
###################################################
# Errors 
###################################################
    
class Error(Exception):
        """
        """
        pass





###################################################
# public
###################################################
#------------------------------------ fake a singleton class
_inst = None
def Focus():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _Focus()
    return _inst
#------------------------------------------------------------





###################################################
# self test 
###################################################

def test():
    """ self test
    """
    focus = Focus()
    focus.homeFocus()
    focus.setFocus(54)
    time.sleep(3)
    try:
        focus.setFocus(300)
    except:
        print "yo"

    focus.shutdown()

if __name__ == "__main__":
    """
    """
    test()  
