#!/usr/local/bin/python
import shutil,os


if __name__ == "__main__":
	root = "/home/skyprobe/datasys/OTIS/otis/src/python/"
	
	paths = []
	files = []
	
	for line in file("commit_list"):
		if line[0] == "#": continue
	
		(file, path) = line.split()
		fullpath = root+path+"/"
		a = os.system("diff "+file+" "+fullpath+file+"\n")
		if a == 0: continue
	
		print "cp",file,fullpath+file
		shutil.copy(file, fullpath+file)	
		paths.append(fullpath)
	       	files.append(file)
	
	batchFile = open("cvsbatch","w")
	
	for i in range(len(paths)):
		print >>batchFile, "cd "+paths[i]
		print >>batchFile, "cvs add "+files[i]	
		
