#!/usr/local/bin/python
"""
Imaging Sky Probe server.  This is the grunge work in starting the
server.  As an option the process may be forked to a daemon.  The
CORBA interface is initialized.  Messages and errors are logged
to the corba log.

Ben Granett
"""

#######################################################
# All settings are in SkyProbeConf
# Parameters are encoded in a dictionary and accessed
# with a string key.
#######################################################
from SkyProbeConf import config

# otis structures are in here...
from OtisTypes import *


import sys, fcntl, signal, os, time

#from Fnorb.orb import BOA, CORBA   # use fnorb
#import OtisCORBA_skel              # import the stubs

from omniORB import CORBA
import OtisCORBA__POA

import Otis
import Log

import daemonted
import ImSkyProbe

verb = True



def printUsage(argv):
    """
    print usage statement
    """
    print "Usage:"
    print argv[0],"[-h, -d, -r]"
    print "########## Options ##############"
    print "-h         print this message"
    print "-s[im]     run skyprobe in simulation mode"
    print "-d[aemon]  turn into a daemon process"
    print "-r[estart] kill the running server and start again"
    print "-b[ug]     turn on extra debug info to stderr"
    print ""
    print "Start the Imaging Sky Probe control server."
    print "The server may be conversed with over a CORBA interface"
    

if __name__ == "__main__":
	""" Parse the command line"""
	
	daemonize = 0
	restart = 0
	
	for i in range(1, len(sys.argv)):
	    opt = sys.argv[i].lstrip("-")
	    if opt[0] == "h":
	        printUsage(sys.argv)
	        sys.exit(0)
	        continue
	    if opt == "s" or opt == "sim":
		config['SimSkyProbe'] = True
		print "SkyProbe running in simulation mode."
	    if opt == "d" or opt == "daemon":
	        daemonize = 1
	        continue
	    if opt == "r" or opt == "restart":
	        restart = 1
	        continue
            if opt == "b" or opt == "bug":
                config['debug'] = True
	    # catch unrecognized options
	    print "unrecognized option:",sys.argv[i]
	    print ""
	    printUsage(sys.argv)
	    sys.exit(1)
	

        """ Read configuration variables. """
        try:
            lockFileName = config['LockFileName']
            iorFileName = config['IORFileName']
            startCmd = config['StartCmd']
        except KeyError:
            print >>sys.stderr, "Configuration error"
            sys.exit(1)
            
	
	""" See if I should restart """
        
	if restart:
	    print "restarting..."
	    try:
	        lockFile = file(lockFileName, 'r')
	        pid = int(lockFile.read().strip())
	        lockFile.close()
	    except IOError:
	        print >>stderr, "could not get pid of current server from",lockFileName
	        sys.exit(1)
	
	    print "gonna kill", pid
	    try:
	        while 1:
	            os.kill(pid, signal.SIGTERM)
	            time.sleep(1)
	    except OSError, err:
	        err = str(err)
	        if err.find("No such process") > 0:
	            pass
	        else:
	            print str(err)
	            sys.exit(1)
	
	    print "all set"
	    print "here we go"
	    # not sure if this is good, but
	    # call myself from the shell...
	    os.system(startCmd)
	    os._exit(0)
	    
	
	""" turn into a daemon """
	if daemonize:
	    res = daemonted.createDaemon(config)
	    if res != 0:
	        print "could not tear myself off with a fork"
	        sys.exit(1)
	
	
	"""
	 use a lock file to ensure we are the only
	 skyprobe running
	"""
	
	# create the lock file if it doesn't exist
	if not os.path.exists(lockFileName):
	    lockFile = open(lockFileName, "w")
	    lockFile.close()
	
	lockFile = open(lockFileName, "r+")
	# see if we can lock it, if not, it is already locked
	# so quit
	try:
	    fcntl.flock(lockFile.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
	except IOError:
	    print sys.argv[0],"already running with pid",lockFile.readline()
	    lockFile.close()
	    os._exit(0)
	
	# print our PID to the file
	lockFile.write(str(os.getpid()))
	lockFile.flush()
	
	
        try:
            orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
        except:
            pass
        poa = orb.resolve_initial_references("RootPOA")
        serverInst = ImSkyProbe.SPServer(orb)
        obj = serverInst._this()
        poaManager = poa._get_the_POAManager()
        poaManager.activate()

	# Get the IOR string
	iorString = orb.object_to_string(obj)

        # write out the IOR to a file just in case
        iorFile = open(iorFileName, "w")
        iorFile.write(iorString)
        iorFile.close()
	
	# register with the name server
	Otis.registerIOR(serverInst.getName(),iorString)
	
	
        print "Imaging SkyProbe up"
	if config['SimSkyProbe']:
	    Log.entry("Aloha from the SImSkyProbe!", 3)
	else:
	    Log.entry("Aloha from the Imaging SkyProbe!", 3)


        #####################################################
        # initialize the hardware
        #####################################################
        ImSkyProbe.init()
	
        try:
            orb.run()
        except KeyboardInterrupt:
            print "got a cont-c!"
            print "Imaging SkyProbe is down"
            Log.entry("Imaging SkyProbe server has shutdown.", 3)
            # remove entry from name server
            Otis.removeIOR(serverInst.getName())
            serverInst.shutdown(1)

	
	
	# unlock the lockfile
	lockFile.close()
        # delete files
        os.unlink(lockFileName)
        os.unlink(iorFileName)
