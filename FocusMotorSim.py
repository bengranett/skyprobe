#!/usr/local/bin/python

"""
Python interface to run a pontech stepper control board.

April 2006   Ben Granett   code is made pretty and functional
June 1 2005  Ben Granett   created

"""

import sys
import time

class Motor:
    """
    Control your pontech stepper motor board (over a serial line.)
    """
    # constants
    FULLSTEP = 1
    HALFSTEP = 2

    FULLSTEPSIZE = 1.8
    HALFSTEPSIZE = 0.9
    
    POWERON  = 1
    POWEROFF = 0

    # default configuration parameters.  These can be set at runtime with the optional
    # config dictionary argument.
    defaults = {'PontechDev': "/dev/ttyUSB2",
                
                'PontechLowLimit': 5,       # The pin that the low limit switch is connected to.
                'PontechHighLimit': 6,      # Pin for high limit switch.
                'PontechLowPinMask': 2,     # Mask to AND with pin bits to read low pin (2=0010)
                'PontechHighPinMask': 4,    # Mask to read high pin (4=0100)
                
                'PontechStepDelay': 10000,
                'PontechStepAccel': 0,
                'PontechStepMin': 0,
                'PontechMoveTimeOut': 5,
                'PontechPowerMode': POWEROFF,
                'PontechStepMode': HALFSTEP,
                'PontechBoardID': 1
                }
                
    
    def __init__(self, config=None):
        """Initialize a new motor instance and opens connection with terminal device.
        Configuration settings can be loaded in via the config dictionary.
        """
        # if a configuration dictionary was passed in, load the parameters.
        if config: self.loadConfig(config)
        
        # open and initialize the port
        self.open()

    def loadConfig(self, config):
        """
        Load configuration values from an input config dictionary.  If the
        dictionary contains keys that are in the local defaults dict, the
        defaults will be overwritten.  No sanity checks are done, so double
        check the values you are passing in.
        """
        for key in self.defaults.keys():
            if config.has_key(key):
                self.defaults[key] = config[key]
                
        
    def open(self):
        """Open the serial port to talk to the motor."""
	# Its dangerous for the motor to start without user initiation?
        # lets just set the counter to 0 and let the user home when they are ready.
        self.pos = 0
        
        # set the step delay.
        self.setDelay(self.defaults['PontechStepDelay'])

        # set the acceleration.
        self.setAccel(self.defaults['PontechStepAccel'], self.defaults['PontechStepMin'])

    def pause(self, x=5):
        """Snooze for 5 thousandths of a second.  Give this command to allow the control
        board time to process a request.
        """
        time.sleep(x/1000.)
        
    def close(self):
        """Close connection to motor.  The motor is stopped, then powered off and finally
        the serial port is closed.
        """
        self.stop()
        self.setPowerMode(self.POWEROFF)
        
    def setBoardID(self, newID):
        """Change the board ID, if you had to.
        """
        if self.id == -1:
            self.id = newID
            self.boardStr = "BD"+self.intStr(self.id)
            return
        if self.id >= 0 and newID >= 0:
            cmd = self.boardStr+"WE0 "+self.intStr(newID)
            #self.term.sendCmd(cmd)
            self.id = newID
            self.boardStr = "BD"+self.intStr(newID)
        return
    
    def getBoardID(self):
        """Return the board ID.  This does not sync with the hardware.
        """
        return self.id

    def waitWhileMoving(self):
        """Returns when the motor has stopped moving, or moveTimeOut has expired.
        Note this function blocks!
        """
        start = time.time()
       
        while self.isMoving():
            if time.time()-start > self.defaults['PontechMoveTimeOut']:
                # If the motor has not stopped moving within the time limit
                # call an emergency stop
                self.stop()
                
                # raise a timeout exception
                current = self.getPos()
                raise TimeOutError(current)

            # take a break before polling again
            self.pause(50)

        

    def isMoving(self):
        """Returns destination position - current position.  When the motor stops
        moving, 0 will be returned.
        """
	return 0

    def setStepMode(self, mode=None):
        """Set the step mode either full or half stepping.
        """
        if mode == self.FULLSTEP or mode == None:
            self.stepMode = self.FULLSTEP
            self.stepSize = self.FULLSTEPSIZE
            string = "SF"
        if mode == self.HALFSTEP:
            self.stepMode = self.HALFSTEP
            self.stepSize = self.FULLSTEPSIZE
            string = "SH"
        cmd = self.boardStr+string
        
        #self.term.sendCmd(cmd)

    def getStepMode(self):
        """Check what the step mode is, does not sync with hardware.
        """
        return self.stepMode

    def setPowerMode(self, mode=None):
        """Set the power mode when the motor is not moving.  Windings can be on or off.
        """
        if mode == self.POWEROFF or mode == None:
            self.powerMode = self.POWEROFF
            string = "SO"
        if mode == self.POWERON:
            self.powerMode = self.POWERON
            string = "SP"
        cmd = self.boardStr+string

        #self.term.sendCmd(cmd)

    def getPowerMode(self):
        """Get the power mode, does not sync with hardware.
        """
        return self.powerMode

    def setDelay(self, n = 10000):
        """Set the step delay in microsec.  This is used to control the speed of the motor.
        """
        self.stepDelay = n
        sdStr = "SD"+self.intStr(n)
        cmd = self.boardStr+sdStr
        #self.term.sendCmd(cmd)

    def getDelay(self):
        """Ask the board what the current step delay is and return it.
        """
        cmd=self.boardStr+"RSD"
        #answer = self.term.sendCmd(cmd, reply=1)
        #self.stepDelay = int(answer)
        return self.stepDelay

    def setFreq(self, f = 1):
        """Set the motor speed using the step frequency in kHz.
        """
        sd = int(1000/f/1.6)
        self.stepDelay = sd
        sdStr = self.intStr(sd)
        cmd = self.boardStr+sdStr
        #self.term.sendCmd(cmd)
        
    def setAccel(self, sa, sm):
        """Set the motor acceleration.  There are two parameters.  Look up what they mean.
        """
        self.stepAccel = sa
        self.stepMin = sm
        saStr = "SA"+self.intStr(sa)
        smStr = "SM"+self.intStr(sm)
        cmd = self.boardStr+saStr+smStr
        #self.term.sendCmd(cmd)

    def getAccel(self):
        """Get the current acceleration parameters from the board.
        """
        cmd=self.boardStr+"RSA"
        answer = self.term.sendCmd(cmd, reply=1)
        self.stepAccel = int(answer)
        
        cmd=self.boardStr+"RSM"
        answer = self.term.sendCmd(cmd, reply=1)
        self.stepMin = int(answer)
        
        return self.stepAccel, self.stepMin
    
    def setPos(self, n):
        """Set the position of the motor.  The limit switches are enforced.
        """
        if n < 0:
            return
        current = self.getPos()

        if n == current: return

        if n - current > 0:
            # moving in the positive direction
	    v = +1
        else:
            # moving in the negative direction
	    v = -1

        c = n
	while True:
	    c += v
	    if c == n: 
		break
	    if c > 400: 
		break
	    if c == 0:
		break
	    self.pause(500)

	self.pos = c
	actual = self.pos

	# check if a limit was hit
        limit = self.checkLimits()
        if limit == self.defaults['PontechLowLimit']:
            raise HitLimitError(self.defaults['PontechLowLimit'], actual)
        
        if limit == self.defaults['PontechHighLimit']:
            raise HitLimitError(self.defaults['PontechHighLimit'], actual)

        # check if we are just not where we expect to be
        if not actual == n:
            raise PositionError(expected, current)
               
    def getPos(self):
        """Get the current position from the board.
        """
        return self.pos


    def checkLimits(self):
        """Check if a limit has been hit.
        """
	if self.pos == 0:
            # "i'm at low pin"
            return self.defaults['PontechLowLimit']
        if self.pos > 400:
            # "i'm at high pin"
            return self.defaults['PontechHighLimit']
        return None
        

    def home(self, n=0):
        """Home the stepper motor.  The motor is run to the low limit switch
        and the encoder is zeroed.
        """
        # If we are at the low limit already,
        # first move away and then re-home.
        if self.defaults['PontechLowLimit'] == self.checkLimits():
            print "low limit is set; moving forward 50"
            currentPos = self.getPos()
	    try:
		self.setPos(currentPos + 50)
            except:
                print >>sys.stderr, "Home failed!!"
                raise
            
	try:
	    self.setPos(0)
        except:
            print >>sys.stderr, "Home failed!!"
            raise
        self.pos = 0

    def stop(self):
        """Emergency stop the motor.  Does not deaccellerate.
        """
	pass
        
    def stopSlowly(self):
        """Stop the motor with decelleration.
        """
	pass

    def dump(self):
        """Dump some motor parameters to stdout.
        """
        print "----------------"
        print "motor ID ",self.id
        print "is it moving?",self.isMoving()
        print "step mode  = ",self.getStepMode()
        print "step power = ",self.getPowerMode()
        print "step pos   = ",self.getPos()
        print "step delay = ",self.getDelay()
        print "step SA,SM = ",self.getAccel()
        print "input pins = ",self.readPins()
        print "analog vol = ",self.readVoltage(1), self.readVoltage(2)

#****************************************************************
#
# Error definitions
#
#****************************************************************
class Error(Exception):
    """ A generic error class."""
    message = "Something bad happened!\n"

class TimeOutError(Error):
    """If the motor does not stop moving within the specified time, there
    is probably something wrong and this exception should be raised.
    """
    def __init__(self, current):
        self.message = "The focus motor timed out before reaching position.  Current position: %i"%(current)

class HitLimitError(Error):
    """Raise this exception when a limit switch is hit unexpectedly."""
    def __init__(self, limit, current):
        self.message = "The focus motor hit a limit. (pin %i) Current position: %i"%(limit, current)

class PositionError(Error):
    """Raise when the motor is not where it was supposed to go."""
    def __init__(self, expected, current):
        self.message = "The focus motor was aiming for %i but stopped at %i."%(expected, current)

#****************************************************************
#
# Test code
#
#****************************************************************
def test():
    print "motor.py  ...running test"
    m = Motor()
    try:
        m.home()
    except TimeOutError:
        print TimeOutError.message

    try:
        m.setPos(400)
    except HitLimitError, error:
        print error.message

    try:
        m.setPos(0)
    except HitLimitError, error:
        print error.message

        
   
    
    
if __name__=='__main__':
    test()
    
