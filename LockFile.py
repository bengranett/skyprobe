""" lock file utility
"""

import sys, os, fcntl

class LockFile:
    """
    """
    def __init__(self, lockFileName):
        """
        """
        self.lockFileName = lockFileName
        self.create()

        
    def create(self):
        """
        """
        # create the lock file if it doesn't exist
        if not os.path.exists(self.lockFileName):
            self.lockFile = open(self.lockFileName, "w")
            self.lockFile.close()
	
        self.lockFile = open(self.lockFileName, "r+")
        # see if we can lock it, if not, it is already locked
        # so quit
        try:
            fcntl.flock(self.lockFile.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            self.lockFile.close()
            raise CantLock(self.lockFileName, os.getpid())
           	
        # print our PID to the file
        self.lockFile.write(str(os.getpid()))
        self.lockFile.flush()
    
        # we have aquired the lock
        

    def close(self):
        """ unlock the lockfile
        """
        try:
            self.lockFile.close()
            os.unlink(self.lockFileName)
        except:
            print "can't close lock file",self.lockFileName
            

class CantLock(Exception):
    def __init__(self, fileName, pid):
        print >>sys.stderr, "Can't aquire lock,",fileName,"\nPID",pid,"already running.\n"




def test():
    lockFileName = "/home/skyprobe/test.lock"
    try:
        lockFile = LockFile(lockFileName)
    except CantLock:
        print sys.argv[0],"already running with pid",lockFile.readline()
        os._exit(0)

    try:
        lockFile2 = LockFile(lockFileName)
    except CantLock:
        pass
    
    lockFile.close()
 

if __name__=="__main__":
    test()
