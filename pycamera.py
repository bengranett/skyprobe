#!/usr/local/python
"""Python interface to the apogee linux driver.

May 17: Rewrote the temperature control stuff.  Now a thread monitors the temperature.  Not yet tested.
commented code may be deleted once everything works i guess...

To do: make status dict thread safe... and add elapsed time here rather than in level above.
"""

import sys, os, time, math
import threader, threading
import pyfits, OSUtils, astrotime
import Utilities
from SkyProbeConf import config
import constants as const
import Log
import SkyProbeDB
import queryScope



#########################################
# load the apogee python module
if not config['SimSkyProbe']:
    sys.path.append(config['CCDApogeeLib'])
    import apogee_usb as ccd
#########################################



class pyCamera:
    """Multithreaded interface to the apogee linux driver.
    """
    
    status = {"ApogeeStatus":  0,
              "camPresent":    False,
              "status":        const.IDLE,
              "startTime":     0,
              'readOutMode':   const.SIXTEENBITSLOW,
              'ElapsedTime':   0,
              'ExposureTime':  0,
              'tstart':        None,

              'Binning':       1,
              'Gain':          1.0,
              'ReadNoise':     10.0,
              'depth':         16,
              'Saturate':      0,
              'BadLevel':      0,
              'ROI':           (0,0,0,0),
              'ImagingRows':   2048,
              'ImagingCols':   2048,
              'OverscanCols':  50,

              'LastRead':      0,
              'CCDTemp':       0,
              'HeatSinkTemp':  0,
              'CoolerDrive':   0,
              'CoolerSetPoint':0,
              'CoolerBackOff': 0,
              'CoolerStatus':  0,

              'CoolerOn':      False,
              'ChangingTemp':  False,
              'CommandTime':   time.time(),
              }
    # create a lock so we can make the status dict thread safe... just in case
    statusLock = threading.Lock()
    
    # locks to control access to threaded exposure and temperature commands
    exposeLock = threading.Lock()
    coolerLock = threading.Lock()
    
    defaults = {'CCDDataDir':"/www/images/",         # Where to save images... trailing slash is important
                'CCDFitsHeader': "Default.hdr",
                'CCDBiasName': "bias.fits",
                'CCDDarkName': "dark.fits",
                'CCDSnapName': "snap.fits",
                'CCDDefaultImageName': "expose.fits",
                'CCDFitsExtension': ".fits",

                'CCDImagingRows': 2048,
                'CCDImagingCols': 2048,
                'CCDDigitizeOverscan': 1,
                'CCDOverscanCols': 50,
                'CCDCoolerTimeOut': 3600,
                'CCDBinMode': 1,
                'CCDDepth': 16,

                'CCDPollTime': .5,   # poll the camera every half second

                'CCDExposeType': {"BIAS": "BIAS",
                                  "DARK": "DARK",
                                  "LIGHT": "LIGHT",
                                  "FLAT": "FLAT",
                                  "TEST": "TEST"},

                'CCDTempLog': 'ccdtemp.log',
                'CCDDefaultSetPoint': -5

                }


    
#------------------------- command classes for the thread pool to run
    class TempMonitorCmd(threader.Command):
        """Run a thread to monitor the CCD temperature"""
        def __init__(self, parent):
            self.parent = parent
        def execute(self):
            try:
                self.parent._tempMonitor()
            except:
                print "Caught exception while setting CCD temp..."
                raise

            
    class exposeCmd(threader.Command):
        """A command class for threader to queue up an exposure.
        """
        def __init__(self, parent, expt, fileName, type=None, info=None):
            self.expt = expt
            self.fileName = fileName
            self.type = type
            self.info = info
            self.parent = parent
        def execute(self):
            try:
                self.parent._expose(self.expt, self.fileName, self.type, self.info)
            except:
                print "Caught exception while exposing..."
                raise
            
#------------------------------------------------------------------
    def __init__(self, camPresent = True):
        """Initialize the camera driver

        Keyword arguments:
        camPresent -- boolean to specify if a camera is attached.
        
        """
        # instantiate the camera
        self.camera = ccd.CApnCamera()
    
        # Assume a camera is attached...
        self.status['camPresent'] = camPresent
	if config['SimSkyProbe']: self.status['camPresent'] = False


        # an event to go off when we need to shutdown.
        self.shutdownEvent = threading.Event()
        self.shutdownEvent.clear()

        # create a thread pool with two threads...
        # at most we will be setting the temp
        # and exposing at the same time.
        self.ThreadPool = threader.threadPool(2)

        # everythihng after this point depends on having
        # a camera attached to the computer
        if not self.status['camPresent']:
            Log.entry("Operating without a camera attached.")
            

        # init driver?
        # This also checks if the camera is connected properly
        camNum = 1
        if not self.camera.InitDriver(camNum, 0, 0) and self.status['camPresent']:
            self.status['camPresent'] = False
            Log.error("Could not connect to camera.", 2)
            #raise IOError, "No camera connected!"
            
        if not self.status['camPresent']:
            Log.entry("SkyProbe operating without the camera!")
            self.status['status'] = const.ERROR

        # load camera settings.
        Utilities.loadConfig(self.defaults, config)

        if self.status['camPresent']:
            self.initCamera()

            # initialize the fits header.
            self._initHeader()

            # set the default set point
            self.setTemp(self.defaults['CCDDefaultSetPoint'])

            # start the temp monitor thread
            self.startTempMonitor()

            self.abortExposure = threading.Event()
            self.killCooler = threading.Event()
        
            # reset and flush the camera for good measure.
            self.camera.ResetSystem()

            Log.entry("Camera initialized.")
            
    def shutdown(self):
        """Clean up the thread pool!  Must be called before exit.
        """
        # turn off the cooler
        self.setCooler(False)
        # quit the temp monitor
        self.shutdownEvent.set()
        # kill the thread pool
        self.ThreadPool.shutdown()
        Log.entry("Camera successfully shutdown.")
                
    def initCamera(self):
        """Initialize the camera."""
        if not self.status['camPresent']:
            return
        
        self.camera.m_DigitizeOverscan = self.defaults['CCDDigitizeOverscan']

        # set up the geometry
        self.defaults['CCDOverscanCols'] = self.camera.read_OverscanColumns()

        # use the full chip including overscan columns
        if self.defaults['CCDBinMode'] == 2:
            self.setTwoBinning()
        else:
            self.resetGeometry()

        # set CCD depth * you should use 16 *
        self.setDepth(self.defaults['CCDDepth'])



    def _tempMonitor(self):
        """Monitor the ccd temperature forever.
        keep track of whether or not the cooler is on,
        if it is maintaining temperature or
        if it is trying to cool up or down

        Try to detect problems, like the temperature is never reached after a really long time...
        """
        # check the temperatures
        self.getTemp()
        
        oldState = self.camera.read_CoolerStatus()
        while not self.shutdownEvent.isSet():
            time.sleep(self.defaults['CCDPollTime'])
            
            # update temperature status
            self.getTemp()

            if self.status['CoolerOn'] and self.status['ChangingTemp']:
                if (time.time() - self.status['CommandTime'] > self.defaults['CCDCoolerTimeOut']):
                    Log.error("CCD Cooler timed out before temperature reached!  Current: %f, Goal: %f"
                              %(self.status['CCDTemp'], self.status['CoolerSetPoint']))
                    self.status['ChangingTemp'] = False

            
            s = self.camera.read_CoolerStatus()
            if s == oldState: continue

            # if the state changed, send a message to the log if it is interesting
            oldState = s
            if s == const.CCDCoolerAtMin:
                self.logTemp(mesg="CCD cooler hit minimum temperature.")
            elif s == const.CCDCoolerAtMax:
                self.logTemp(mesg="CCD cooler hit max temperature (!)")
            elif s == const.CCDCoolerAtAmbient:
                self.logTemp(mesg="CCD cooler reached ambient temperature.")
            elif s == const.CCDCoolerAtSetPoint:
                self.logTemp(mesg="CCD cooler reached set point.")
                success = 1
                
        Log.entry("CCD cooler monitor thread stopped!")
        
    def startTempMonitor(self):
        """ Start a thread to monitor the temperature.
        """
        cmd = self.TempMonitorCmd(self)
        self.ThreadPool.do(cmd, False)

    def coolerIsOn(self):
        """Ask if the cooler is on or off"""
        if self.status['CoolerStatus'] == 0:
            return False
        return True

    def setCooler(self, status):
        """Set the cooler on or off.
        It is a good habit to ramp the temperature to ambient before turning off the cooler.
        """
	if status:
            if self.coolerIsOn(): return
            # turn the cooler on
            if self.status['camPresent']: 
		self.camera.write_CoolerEnable(True)
            self.status['CoolerOn'] = True
            Log.entry("CCD cooler is now on!  Aiming for %f"%(self.status['CoolerSetPoint']))
        else:
            if not self.coolerIsOn(): return
            # or turn it off...
	    if self.status['camPresent']: 
		self.camera.write_CoolerEnable(False)
            self.status['CoolerOn'] = False
            Log.entry("CCD cooler is now off!  Current temp:%f\n(Did you ramp the temperature to ambient?  Did you leave the lawn mower running?)"%(self.status['CCDTemp']))



    def setTemp(self, temp):
        """Set the temperature goal."""
        if not self.status['camPresent']:
            return
        
        # check if the temperature makes sense
        if temp < -40 or temp > 25:
            raise TemperatureError("CCD temperature you specified is out of range:%f"%(temp))

        # set the set point
        self.camera.write_CoolerSetPoint(temp)
        self.status['CoolerSetPoint'] = temp
        self.status['CommandTime'] = time.time()

        
    def logTemp(self, logFile = None, mesg = None):
        """write out a line to a log file with the temp info

        Arguments
        logFile -- a file name to use for the log file.  If None, a default
        file name will be used.
        mesg    -- a string to write out to the log file in addition to the
        temperature data.
        
        """
        if not self.status['camPresent']: return
        
        if logFile != None:
                self.tempLog = logFile
        if self.tempLog == None: self.tempLog = "TempLog.ascii"

        log = open(self.tempLog, "a")
        t = int(time.time())
        if mesg != None:
            print >>log, t,mesg
            print t, mesg

        ccdtemp = self.camera.read_TempCCD()
        hstemp  = self.camera.read_TempHeatsink()
        status  = self.camera.read_CoolerStatus()
        drive   = self.camera.read_CoolerDrive()
        setpoint= self.camera.read_CoolerSetPoint()
        backoff = self.camera.read_CoolerBackoffPoint()
        
        print >>log, t, status, ccdtemp, hstemp, drive, setpoint, backoff
        print t, status, ccdtemp, hstemp, drive, setpoint, backoff
        log.close()



    def getTemp(self):
        """Read the current temperature.
        Now use getstatus to lookup the values
        """
        if not self.status['camPresent']:
	    self.statusLock.acquire()
	    self.status['CCDTemp'] = 0.0
	    self.status['HeatSinkTemp']  = 0.0
	    # self.status['CoolerStatus']  = 0
	    # self.status['CoolerDrive']   = 0
	    # self.status['CoolerSetPoint']= 0
	    # self.status['CoolerBackOff'] = 0
	    self.status['LastRead'] = time.time()

	    if self.coolerIsOn():
		self.status['CoolerOn'] = True
	    self.statusLock.release()

            return

        self.statusLock.acquire()
        self.status['CCDTemp'] = self.camera.read_TempCCD()
        self.status['HeatSinkTemp']  = self.camera.read_TempHeatsink()
        self.status['CoolerStatus']  = self.camera.read_CoolerStatus()
        self.status['CoolerDrive']   = self.camera.read_CoolerDrive()
        self.status['CoolerSetPoint']= self.camera.read_CoolerSetPoint()
        self.status['CoolerBackOff'] = self.camera.read_CoolerBackoffPoint()
        self.status['LastRead'] = time.time()

        if self.coolerIsOn():
            self.status['CoolerOn'] = True
        self.statusLock.release()

    def getStatus(self):
        """return the current camera status and
        calculate the elapsed time if an exposure is in progress."""
        self.statusLock.acquire()
        self.status['ElapsedTime'] = 0
        if self.status['status'] == const.EXPOSING:
            self.status['ElapsedTime'] = time.time() - self.status['startTime']

        self.status['ApogeeStatus'] = self.camera.read_ImagingStatus()

            
        self.statusLock.release()

	# generate a temperature dictionary
	self.status['temperatures'] = {'CCD': self.status['CCDTemp'],
				       'CCD heat sink': self.status['HeatSinkTemp']}

        return self.status
        

# ---------------------------- header routines
    def _initHeader(self):
        """Create an empty header to populate later
        """
        # there is a bug in pyfits preventing the use of an empty header...
        # so create a header with some basic tags
        try:
            # load a default header from a fits file if it exists
            hdu = pyfits.open(self.defaults['CCDFitsHeader'])
            self.header = hdu[0].header
        except:
            # or else use a default with not much in it
            hdu = pyfits.PrimaryHDU()
            self.header = hdu.header
            
    def _mergeHeader(self, fitsFile, newHeader):
        """Merge the new header tags with the fits file.
        """
        hdu = pyfits.open(fitsFile, "update")
        for i in range(len(newHeader.items())):
            # If the tag already exists in the file, don't overwrite
            if hdu[0].header.has_key(newHeader.items()[i][0]): continue

            print "Header... adding",newHeader.items()[i]
            # add the new tag
            hdu[0].header.ascard.append(newHeader.ascard[i])

        # write out the updated header to file
        hdu.flush()
# -----------------------------------------------

    def bias(self, fileName=None, info=None, block = True):
        """Take a bias exposure.
        """
        if not fileName: fileName = self.defaults['CCDBiasName']
        self.expose(0.0, fileName=fileName, type=self.defaults['CCDExposeType']['BIAS'], info=info, block=block)

    def dark(self, expTime, fileName=None, info=None, block = True):
        """Take a dark exposure.
        """
        if not fileName: fileName = self.defaults['CCDDarkName']
        self.expose(expTime, fileName=fileName, type=self.defaults['CCDExposeType']['DARK'], info=info, block=block)

    def snap(self, expTime, fileName=None, info=None, block = True):
        """Take a light exposure.
        """
        if not fileName: fileName = self.defaults['CCDSnapName']
        self.expose(expTime, fileName=fileName, type=self.defaults['CCDExposeType']['LIGHT'], info=info, block=block)

    def ready(self):
        """Return true if the camera is ready to take an image
        """
        return self.status['ApogeeStatus'] >= 0
            
    def _expose(self, expt, fileName=None, type="TEST", info=None):
        """Take an exposure!
        Apogee.h:#define        Apn_Status_DataError -2
        Apogee.h:#define        Apn_Status_PatternError  -1
        Apogee.h:#define        Apn_Status_Idle  0
        Apogee.h:#define        Apn_Status_Exposing  1
        Apogee.h:#define        Apn_Status_ImagingActive  2
        Apogee.h:#define        Apn_Status_ImageReady  3
        Apogee.h:#define        Apn_Status_Flushing  4
        Apogee.h:#define        Apn_Status_WaitingOnTrigger 5
        Apogee.h:#define        Apn_Status_ConnectionError 6

        """
        # only one at a time please
        if not self.exposeLock.acquire(False):      # False means no block
            Log.error('Unable to claim cameralock!', 2)
            return
        
        self.readout = True


        # Check if the camera is ready
        if not self.ready():
            self.statusLock.acquire()
            self.status['status'] = const.ERROR
            self.status['camPresent'] = False
            Log.error('Camera is in error state '+str(self.status['ApogeeStatus']), 2)
            self.statusLock.release()
            self.exposeLock.release()
            return



        # Set some status variables
        t0=time.time() # remember the start time



        self.statusLock.acquire()
        self.status['status'] = const.EXPOSING
        self.status['ExposureTime'] = expt
        self.status['startTime'] = t0
        self.statusLock.release()
        
        if not self.status['camPresent']:
            # if there is no camera, just sleep i guess.
            Log.error('Camera is not enabled: status: '+str(self.status['ApogeeStatus']), 2)
            time.sleep(expt)
            self.exposeLock.release()
            print "exposure finished"
            
            self.statusLock.acquire()
            self.status['status'] = const.IDLE
            self.statusLock.release()
            return


        # use a default filename if none was supplied
        if not fileName: fileName = self.defaults['CCDDefaultImageName']


        # If fileName is not a complete path, stick in the data directory in front
        if not "/" in fileName:
            fileName = self.defaults['CCDDataDir'] + fileName
        
        # copy the default header 
        newHeader = self.header.copy()

        # 1 = Shutter open
        # 0 = Shutter closed
        self.status['Shutter'] = 1
        if type == self.defaults['CCDExposeType']['BIAS'] or type == self.defaults['CCDExposeType']['DARK']: self.status['Shutter'] = 0

        logString = "Starting exposure"+"\nExposure time: "+str(expt) +"\ntype: "+str(type)+\
                    "\nShutter: "+str(self.status['Shutter'])+"\nadditional: "+str(info)
        
        Log.entry(logString)
        
        self.abortExposure.clear()   # clear the abort event
  
        # record the start time for inclusion in the header
	seconds = time.time()   # seconds since 1970
	sec = int(seconds)
	nanosec = int((seconds - sec)*1e9)
        self.status['tstart'] = time.gmtime(seconds)   # a list with year mon day in utc
        
        noError = self.camera.Expose(expt, self.status['Shutter'])
        print "exposure error code", noError

        #    self.camera.read_ImagingStatus()
       
        time.sleep(0.1)
        self.abortExposure.wait(timeout = expt)
        
        

        # if we are not reading out the chip, then return
        #if not self.readout:
        #    self.exposeLock.release()
        #    
        #    self.statusLock.acquire()
        #    self.status['status'] = const.IDLE
        #    self.statusLock.release()
        #    return

        self.statusLock.acquire()
        self.status['status'] = const.READING
        self.statusLock.release()

        # put header stuff in a try block in case it crashes
        # crashes in here will kill the camera requiring a power cycle!!
        try:
            # make sure the file has an extension, preferably fits
            # and make it unique by adding numbers to it
            name = OSUtils.findFileName(fileName, self.defaults['CCDFitsExtension']);
            
            # insert everything into the header
            newHeader = self.addHeaderKeywords(newHeader, info)
        except:
            Log.error("Error with the fits keywords")
            
        # buffer the image last
        self.camera.BufferImage()
        
        r = ccd.writeFits(name)
        if r == 105:
            raise FITSError, "Can't write fits file", name

        os.system("cp "+name+" temp")
        self._mergeHeader(name, newHeader)
        
        #print "SUCCESS! image written to file:", name
        #print "time:",time.time()-t0

        self.statusLock.acquire()
        self.status['status'] = const.IDLE
        self.statusLock.release()
        Log.entry("Exposure written to file: "+name)

	#----------------------------------------------------------------------------
	# enter database info
	leap = 0
	try:
	    SkyProbeDB.ispdb().insert(config['MYSQLExposeTable'],
				      config['MYSQLExpoLabels'],
				      (info['SEQID'], info['EXPID'], sec, nanosec, leap,
				       expt, info['FILTER'], info['IMGTYPE'], info['FOCUS']))
	except:
	    Log.error("Error when writing to expose mysql table.")
        #----------------------------------------------------------------------------
        
        
        # we're done, release the lock
        self.exposeLock.release()


    def addHeaderKeywords(self, header, info=None):
        """Put information into the fits header."""
        if info:
            for key in info.keys():
                header.update(key, info[key])

        ######## Exposure info #############
        header.update("EXPTIME", self.status['ExposureTime'])
        header.update("DARKTIME", self.status['ExposureTime'])
        #header.update("IMGTYPE", self.status['ImageType'])
        if self.status['Shutter']:
            header.update("SHUTTER", "OPEN")
        else:
            header.update("SHUTTER", "CLOSED")
        temp = self.camera.read_TempCCD()
        header.update("CCDTEMP", temp)
        
        ######## Time ####################
        # put utc date and time in the header
        # remember we are getting this from the computer clock
        # and the python time.gmtime command...
        tstart = self.status['tstart']
        datestr = str(tstart[0])+"-"+str(tstart[1])+"-"+str(tstart[2])
        header.update("DATE-OBS", datestr)
        timestr = str(tstart[3])+":"+str(tstart[4])+":"+str(tstart[5])
        header.update("TIME-OBS", timestr)

        # calculate the equinox and julian date
        eqx = astrotime.equinox(tstart)
        jd = astrotime.julianDate(tstart)

        header.update("EQUINOX", eqx)
        header.update("JD", jd)
        header.update("MJD-OBS", jd-2400000.5)   # convert to MJD

        ######## Telescope name stuff ######
        header.update("TELESCOP", 'ISP-1')
        header.update("INSTRUME", 'ISP-Apogee')
        header.update("DETECTOR", 'ISP-Apogee-01')


            
        ######## Telescope position ########
        tele = queryScope.telescope()
        scope = tele.queryTelescope()
        header.update("RA", "%10.6f"%(scope['RA']))
        header.update("DEC", "%10.6f"%(scope['DEC']))
        header.update("RADECSYS", scope['RADECSYS'])
        header.update("ALT", "%10.6f"%(scope['ALT']))
        header.update("AZ", "%10.6f"%(scope['AZ']))
        header.update("POSANGLE", "%10.6f"%(scope['POSANGLE']))
        header.update("AIRMASS", "%10.6f"%(scope['AIRMASS']))

        
        ######## CCD statistics ##########
        header.update("ISPCAMER", "Apogee U42")
        #header.update("BITPIX", self.status['depth'])
        header.update("GAIN", "%i"%(self.status['Gain']))
        header.update("RDNOISE", "%i"%(self.status['ReadNoise']))
        header.update("SATURATE", "%i"%(self.status['Saturate']))
        header.update("BADLEVEL", "%i"%(self.status['BadLevel']))
        header.update("TRIMSEC", "[%i:%i, %i:%i]"%(1, self.status['ImagingCols'],
                                                      1, self.status['ImagingRows']))
        header.update("BIASSEC", "[%i:%i, %i:%i]"%(1+self.status['ImagingCols'],
                                                      self.status['OverscanCols']+self.status['ImagingCols'],
                                                      1, self.status['ImagingRows']))
        header.update("XBIN", "%i"%(self.status['Binning']))
        header.update("YBIN", "%i"%(self.status['Binning']))

        ####### ISP version ###########
        header.update("ISPVERS", config['ISPversion'])

        return header
        
    def expose(self, expt, fileName=None, type="TEST", info=None, block=True):
        """Take an exposure and write the data out to a file.
        """
        self.statusLock.acquire()
        self.status['status'] = const.EXPOSING
        self.statusLock.release()

        if not fileName: fileName = self.defaults['CCDSnapName']
        
        cmd = self.exposeCmd(self,expt, fileName, type, info)
        self.ThreadPool.do(cmd, block)
        
    def stopExposure(self, readout=True):
        """Halt the current exposure, and optionally readout the data
        """
        if not self.status['camPresent']: return
        
        Log.entry("Halt the current exposure")
        self.camera.StopExposure(readout)
        self.readout = readout
        self.camera.abortExposure.set()

    def waitWhileReading(self, timeout=15.):
        """ If the camera is doing something, wait for it to finish. """
        tstart = time.time()
        while self.status['status'] == const.READING:
            if time.time() - tstart > timeout:
                raise TimeOutError("Camera timed out waiting for readout to complete.")
            time.sleep(0.1)
        print "********HAd to wait while reading for:",time.time()-tstart


    def waitWhileExposing(self, timeout):
        """wait while an exposure is in progress"""
        print "================ wait while exposing timeout",timeout
        # first wait half a second to be fair
        time.sleep(.5)

        # now start counting!  I do this because if you use the exposure
        # time as the time out, it will be too short.
        tstart = time.time()
        while self.status['status'] == const.EXPOSING:
            if time.time() - tstart > timeout:
                raise TimeOutError("Camera timed out waiting for exposure to complete.")
            time.sleep(0.1)
        print "********HAd to wait while exposing for:",time.time()-tstart
        
#---------------------------------------------------- settings
    def setROI(self, startx, starty, pixx, pixy):
        """Set the CCD region of interest.
        
        Arguments
        startx -- first x coordinate, counts from 0
        starty -- first y coordinate, counts from 0
        pixx   -- columns of region, smallest possible is 1 pixel
        pixy   -- rows of region, smallest is 1 pixel
        
        """
        self.status['ROI'] = (startx, starty, pixx, pixy)

        if not self.status['camPresent']: return
        
        self.camera.write_RoiStartX(startx)
        self.camera.write_RoiStartY(starty)
        self.camera.write_RoiPixelsH(pixx)
        self.camera.write_RoiPixelsV(pixy)

    def resetGeometry(self):
        """Reset the binning to 1 and the ROI to the whole chip.
        """
        self.status['Binning'] = 1
        self.status['ImagingCols'] = self.defaults['CCDImagingCols']
        self.status['ImagingRows'] = self.defaults['CCDImagingRows']
        self.status['OverscanCols'] = self.defaults['CCDOverscanCols']
        if not self.status['camPresent']: return
        
        hBin = 1
        vBin = 1
        
        self.camera.write_RoiBinningH(hBin)
        self.camera.write_RoiBinningV(vBin)

        
        
        self.setROI(0, 0,\
                    self.defaults['CCDImagingCols']+self.defaults['CCDOverscanCols'],
                    self.defaults['CCDImagingRows'])
        
    def setTwoBinning(self):
        """Set 2x2 pixel binning
        """
        self.status['Binning'] = 2
        self.status['ImagingCols'] = self.defaults['CCDImagingCols']/hbin
        self.status['ImagingRows'] = self.defaults['CCDImagingRows']/vbin
        self.status['OverscanCols'] = self.defaults['CCDOverscanCols']/hbin
        if not self.status['camPresent']: return
   
        hBin = 2
        vBin = 2
        
        self.camera.write_RoiBinningH(hBin)
        self.camera.write_RoiBinningV(vBin)

        self.setROI(0, 0,\
                    (self.ImagingCols+self.oscanCols)/hBin, self.ImagingRows/vBin)
        
    def reset(self):
        """Reset (and flush)
        """
        self.flush()

    def flush(self):
        """Flush (and reset)
        """
        if not self.status['camPresent']: return
        
        Log.entry("Camera flushed and reset")
        self.camera.ResetSystem()

    def setDepth(self, depth):
        """Set depth to 12 or 16 bits

        Arguments
        depth -- an integer, either 12 or 16.
        alternatively, depth can be the constant defined by the CORBA IDL
        """
        self.status['depth'] = depth
        if not self.status['camPresent']: return
        
        if depth == 12 or depth == const.TWELVEBITSLOW:
            self.camera.write_DataBits(1)
            self.camera.InitTwelveBitAD()
            return 0
        
        if depth == 16 or depth == const.SIXTEENBITSLOW:
            self.camera.write_DataBits(0)
            self.camera.InitAlta2AD()
            return 0

        return -1

    def setGain(self, gain, offset=None):
        """Set the gain in 12 or 16 bit mode.
        
        Arguments
        gain   -- gain for 12 or 16 bit mode.
        offset -- In 16 bit mode only the offset can be set.
        
        """
        self.status['Gain'] = gain
        if not self.status['camPresent']: return

        if self.camera.m_DataBits:
            # 12 bit mode
            self.camera.write_TwelveBitGain(gain)
            if offset:
                self.camera.write_TwelveBit
        else:
            # 16 bit mode
            self.camera.write_Alta2ADGainSixteen(gain)
            if offset:
                self.camera.write_Alta2ADOffsetSixteen(offset)
    
class Error(Exception):
    """Base exception class.
    """
    pass

class CoolerError(Error):
    """Probably the temperature you tried to set doesn't make sense.
    """
    def __init__(self, mesg):
        """"""
        self.mesg = mesg
    def __str__(self):
        print self.mesg

class IOError(Error):
    """Communication with the camera error.

    Maybe no camera is attached.
    """
    def __init__(self, mesg):
        """
        """
        self.mesg = mesg
    def __str__(self):
        return self.mesg

class FITSError(Error):
    """Can't write the fits file error.
    """
    def __init__(self, mesg, filename=None):
        self.mesg = mesg
        self.filename = filename
    def __str__(self):
        if self.filename != None:
            return mesg+"\nfile: "+filename
        return mesg
    
class HeaderError(Error):
    """Some problem with the fits header error.
    """
    def __init__(self, mesg, filename=None):
        self.mesg = mesg
        self.filename = filename
    def __str__(self):
        if self.filename != None:
            return mesg+"\nfile: "+filename
        return mesg

class TimeOutError(Error):
    """Camera may be hanging"""
    def __init__(self, mesg):
        """
        """
        self.mesg = mesg
    def __str__(self):
        return self.mesg


    
    

def test():
    try:
        cam=pyCamera()
    except:
        cam.shutdown()
        raise
    cam.snap(0)
    t0 = time.time()
    while time.time() < t0 + 5:
        stat = cam.getStatus()
        #print stat
        print stat["CCDTemp"], stat["HeatSinkTemp"]
        time.sleep(1)
    cam.shutdown()

if __name__ == "__main__":
    test()
