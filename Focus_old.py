#!/usr/local/bin/python

"""Lowish level focus commands.
   Runs as a single thread with a queue interface.
"""
import sys, time

import threading, Queue

import Exposure


# Create a lock for the focus
lock = threading.Lock()
#############################

class __Focus():
	"""
	"""
	global lock
	
	# status codes
	BUSY = 1
	READY = 0
	
	status = READY
	
	position = None
	jobQueue = Queue.Queue()

	# command labels
	# jobs are sent to the queue as tuples of the form
	# (label, arg1, arg2, ...)
	QUIT = "quit"
	SETPOS = "setpos"
	ZERO = "zero"
	
	# raise this event to abort a motor movement
	abortEvent = threading.Event()
	abortEvent.clear()

	def run(self):
		""" Wait for jobs to come through the queue.
		    Jobs are tuples of the form
		    (jobLabel, arguments)

		    Three labels are recognized:
		    QUIT   kill this thread
		    SETPOS move the focus ring
		    ZERO   zero the focus ring
		"""
		while 1:
			cmd = self.jobQueue.get()
			
			if cmd[0] == self.QUIT:
				break
			if cmd[0] == self.SETPOS:
				self.__setFocus(cmd[1])
			if cmd[0] == self.ZERO:
				self.__zeroFocus()
	
       	def setLocks(self):
		""" Before we take command of the motor, get the exposure lock
		    to ensure an exposure is not in progress.  We take the
		    focus lock so other threads won't mess with us.
	        """
		lock.acquire()
		Exposure.lock.acquire()

	def releaseLocks(self):
		""" release the locks
		"""
		lock.release()
		Exposure.lock.release()

	def kill(self):
		""" kill this thread
		"""
		cmd = (self.QUIT,)
		self.jobQueue.put(cmd)
	
	def getStatus(self):
		""" return the busy status
		"""
		return self.status

	def abort(self):
		""" abort the current activity
		"""
		print "sending abort to motor"
		self.abortEvent.set()

	def zeroFocus(self):
		""" put the zeroFocus command in the queue
		"""
		cmd = (self.ZERO,)
		self.jobQueue.put(cmd)

	def __zeroFocus(self):
		""" home the focus
		"""
		self.status = self.BUSY
		self.setLocks()
		
		print "zeroing the focus"
		print "sleeping for 10sec"
		self.abortEvent.wait(timeout = 10)
		print "focus zeroed"
		self.position = 0
		
		self.status = self.READY
		self.releaseLocks()

	def setFocus(self, n):
		""" put the setFocus command in the queue
		"""
		cmd = (self.SETPOS,n)
		self.jobQueue.put(cmd)

	def __setFocus(self, n):
		""" set focus to position n
		"""
		self.status = self.BUSY
		self.setLocks()
		
		self.position = n
		print "setting focus to:",n
		print "waiting 10sec"
		self.abortEvent.wait(timeout = 10)
		self.status = self.READY
		self.releaseLocks()

	def readFocus(self):
		"""
		"""
		return self.position
			
###################################################
# Errors 
###################################################
	
class Error(Exception):
        """
        """
        pass





focus = None

###################################################
# Public access
###################################################
def init():
	""" start the thread, we do this
	    cause we should only have a single
	    instance.
	"""
	# create a singleton instance
	global focus
	if focus == None:
		focus = __Focus()
		focus.start()

def kill():
	""" kill the thread, bam
	"""
	focus.abort()
	focus.kill()

def abort():
	""" abort the current activity
	"""
	focus.abort()

def getStatus():
	""" return the status
	"""
	return focus.getStatus()

def readFocus():
	"""Return the current position.
	"""
	return focus.readFocus()

def setFocus(n):
	""" set the focus position
	"""
	focus.setFocus(n)

def zeroFocus():
	""" home the focus
	"""
	focus.zeroFocus()

###################################################
# self test 
###################################################

def test():
	""" self test
	"""
	init()
	Exposure.lock.acquire()

	setFocus(54)
	time.sleep(1)
	Exposure.lock.release()
	
	time.sleep(3)
	abort()

	kill()

if __name__ == "__main__":
	"""
	"""
	test()	
