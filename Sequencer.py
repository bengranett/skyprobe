""" The Sky Probe exposure sequencer.

The sequencer sends jobs out to the filter wheel, focus motor and camera.

"""
import sys, time, pickle
import Queue
# import class definitions
import constants as const
import Log
import threader
import Focus, FilterWheel, Camera
import SkyProbeDB, OSUtils
from SkyProbeConf import config


class _Sequencer:
    """
    """
    params = {'expSeq': None,
	      'targetType': const.DOME,
              'targetTypeStr': str(const.DOME),
	      'seqID': "",
	      'seqBaseStr': "",
	      'seqExtra': None,
	      'resetSeqID': True,
	      }
    
    try:
        params['expSeq'] = config['SequencerDefault']
    except KeyError:
        params['expSeq'] = [("dome", 0, -1)]

    def __init__(self): 
        """Initialize the exposure queue.
        """
        self.exposureQueue = Queue.Queue()
        self.sequencerThreadPool = threader.threadPool(5)
        self.Foc = Focus.Focus()
        self.FW = FilterWheel.FilterWheel()
        self.camera = Camera.Camera()

    def setID(self, id, reset=True):
        """
        """
        if id == "":
	    raise BadIDError("Sequence ID supplied is empty!")

        print "setting id to", id

	# query the database... to check sequence ID
	if SkyProbeDB.ispdb().exists(config['MYSQLSeqTable'], 'id', id):
            print "that seq ID is not unique sorry"
	    raise BadIDError("Sequence ID is not unique! "+id)
        
	self.params['seqID'] = id
	self.params['resetSeqID'] = reset
	

    def setIDBase(self, basestr):
        """
        """
	self.params['seqBaseStr'] = basestr
        print "setting base id to", basestr

	# generate a unique sequence ID from this base
	seqID, extStr = self.newSeqID(basestr)
	self.params['seqExtra'] = extStr

        print "unique name:",seqID, extStr

	try:
	    self.setID(seqID, reset=False)
	except:
            print "didnt like that name"
	    raise
	
	
    def newSeqIDOld(self, base):
	"""Make a unique sequence ID for a base string"""
	try:
	    histFileID = file(config['seqHistFile'],'r')
	    baseHistory = pickle.load(histFileID)
	    histFileID.close()
	except:
	    Log.entry("New sequence base string file started!")
	    baseHistory = {}

	try:
	    count = baseHistory[base] + 1
	    baseHistory[base] = count
	except:
	    count = 0
	    baseHistory[base] = count

	# dump the data back to the file
	try:
	    histFileID = file(config['seqHistFile'],'w')
	    pickle.dump(baseHistory, histFileID, -1)
	    histFileID.close()
	except:
	    log.error("Pickling failed!  Cannot write to file: " + config['seqHistFile'])
	
	# generate the sequence ID
	# if this base has never been used, I don't need to append anything to it
	if count == 0:
	    return base, ''
	ext = config['seqExtFormatStr']%(count)
	seqID = base + ext
	return seqID, ext

    def newSeqID(self, base):
        """Make a new sequence id from a base ID.
        Looks in the mysql table for historical base strings"""
        try:
            data = SkyProbeDB.ispdb().select('sky_probe_sequence',['ext',('base','=',base)])
        except:
            Log.error("Can't access mysql table 'sky_probe_sequence'")
            return base, ""

        # if the base has not been used before, just do it
        if len(data) == 0:
            return base, ""

        # if it has been used, we need to imcrement a counter
        list = []
        for entry in data:
            ext = entry[0]
            i, num, root = OSUtils.findNumInString(ext)
            list.append(num)

        list.sort()
        newNum = list[-1]+1
        newExtStr = config['seqExtFormatStr']%(newNum)
        seqID = base + newExtStr
        
        return seqID, newExtStr
        
                                  

    def loadSeq(self, sequence):
        """ Load an exposure sequence
        """
        for exp in sequence:
            # check that we recognize the filter
            filter = exp.filter
            if not self.FW.validateFilter(filter):
                Log.error("Input sequence has bad filter name: %s"%(filter))
                raise const.OtisCORBA_BadCommandException
                
        self.params['expSeq'] = sequence

    class goCmd(threader.Command):
        """
        """
        def __init__(self, parent, seqID, baseStr, extStr, monitor):
	    self.seqID = seqID
	    self.baseStr = baseStr
	    self.extStr = extStr
            self.parent = parent
            self.monitor = monitor

        def execute(self):
            try:
                self.parent._go(self.seqID, self.baseStr, self.extStr, self.monitor)
            except:
                Log.error("Exception caught while executing sequence.", 2)
                raise

    def go(self, monitor = None):
        """ Start a thread to process the exposure queue.
        """
	seqID = self.params['seqID']
        baseStr = self.params['seqBaseStr']
        extStr = self.params['seqExtra']


	# check if no ID has been supplied
	if not seqID:
	    raise IDExpiredError("The sequence ID must be set before we go.")

	# We now have a sequence ID, hit it!
        cmd = self.goCmd(self, seqID, baseStr, extStr, monitor)
        self.sequencerThreadPool.do(cmd, block = False)


        # write to the mysql sequence table
	try:
	    SkyProbeDB.ispdb().insert(config['MYSQLSeqTable'],
				      config['MYSQLSeqLabels'],
				      (seqID, baseStr, extStr))
	except:
	    Log.error("Error when writing to sequence mysql table.")

	
	# reset the sequence ID if setID was called prior to go
	if self.params['resetSeqID']:
	    self.params['seqID'] = None
        else:
            # If base id is being used, prepare for the next sequence
            self.setIDBase(self.params['seqBaseStr'])

        return cmd
 
    def _go(self, seqID, baseStr, extStr, monitor):
        """Process the exposure queue.
        """
        Log.entry("Starting exposure sequence.")
        # Get control of the hardware
        if not self.Foc.lock():
            Log.error("Can't acquire focus motor lock, aborted.")
            if monitor:
                monitor.setError("go: Focus motor in use.")
            return
        if not self.FW.lock():
            Log.error("Can't acquire filter wheel lock, aborted.")
            self.Foc.unlock()
            if monitor:
                monitor.setError("go: Filter wheel in use.")
            return
        if not self.camera.lock():
            Log.error("Can't acquire camera lock, aborted.")
            self.Foc.unlock()
            if monitor:
                monitor.setError("go: camera in use.")
            return

	expID = 1
        for exp in self.params['expSeq']:
	    exp.expID = expID
	    exp.seqID = seqID
            self.process(exp)
	    expID += 1

        
        try:
            self.camera.waitWhileReading(timeout = 15)
        except:
            Log.error("Camera may have hung readingout")


        self.camera.unlock()
        self.Foc.unlock()
        self.FW.unlock()
        if monitor:
            monitor.setDone()
            
        return


    def process(self, exp):
        """Execute an exposure command
        """
        global config
        
        filter = exp.filter
        expt = exp.duration
        focus = exp.focus
	seqID = exp.seqID
	expID = exp.expID
	filename = config['fileNameFormatStr']%(seqID, expID)

        #Log.entry("processing exposure: %s %f %i %s"%(filter,expt,focus, config['FilterCodes']['DARK']))

        # start the filter wheel and focus commands asynchronously
        cmd1 = self.FW.setFilter(filter, block = False)
        cmd2 = self.Foc.setFocus(focus, block = False)
        # wait for these commands to finish
        cmd1.wait()
        cmd2.wait()

        # set the target
        # make sure this is a string!!
	type = self.params['targetTypeStr']

        #Log.entry("type = %s"%(type))
        
        FilterStatus = self.FW.getStatus()
        FocusStatus = self.Foc.getStatus()
        
	# make a dictionary to hold keywords to put into the fits header
	# this is also used to store info that will be sent to mysql
        headerInfo = {'IMGTYPE': type,
                      'FILTNAME': FilterStatus[1],
                      'FILTER': FilterStatus[2],
                      'FOCUS': FocusStatus[1],
		      'SEQID': seqID,
		      'EXPID': expID
                      # other header info should be added in here
                      # like RA DEC and SECZ
                      }

        # wait for last exposure to readout if it hasn't
        try:
            self.camera.waitWhileReading(timeout = 15)
        except:
            Log.error("Camera may have hung reading out")

        # print ">>>>>>>>starting exposure", time.time()
        # start the exposure and immediately return
        self.camera.expose(expt, fileName=filename, header=headerInfo, block = False)

        # print ">>>>>>>>>exposure started",time.time()

        # wait while the exposure is in progress, but not for readout
        try:
            self.camera.waitWhileExposing()
        except:
            Log.error("Exposure never completed!")

        time.sleep(1)
        # print ">>>>>>>>>exposure finished",time.time()
        

    def setTarget(self, type):
        """ What are we looking at?  
        Indicates the type of data taken by subsequent exposures
        as either night sky, twilight flat, dome flat, or focus sequence.
        """
        self.params['targetType'] = type
        self.params['targetTypeStr'] = str(type)

    def getStatus(self):
        """
        """
        return self.params

    def shutdown(self):
        """
        """
        Log.entry("sequencer shutting down.")
        self.sequencerThreadPool.shutdown()


#------------------------------------ Exceptions

class Error(Exception):
    def __init__(self, mesg):
	""" """
	self.mesg = mesg
    
    def __str__(self):
	return self.mesg


class BadIDError(Error):
    pass

class IDExpiredError(Error):
    pass

 
#------------------------------------ fake a singleton class
_inst = None
def Sequencer():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _Sequencer()
    return _inst
#------------------------------------------------------------


def testSeqID():
    print newSeqID("base")
    print newSeqID("base")
    print newSeqID("base2")
    print newSeqID("base2")


def test():
    fo = Focus.Focus()
    fi = FilterWheel.FilterWheel()
    camera = Camera.Camera()
    seq = Sequencer()
    
    exp1 = const.Exposure("g",1, -1)
    exp2 = const.Exposure("r",1, 100)
    exp3 = const.Exposure("i",1, 10)
    s = [exp1]#, exp2, exp3]
    seq.loadSeq(s)
    cmd=seq.go()
    cmd.wait()
    
    print "shutting down"
    fo.shutdown()
    fi.shutdown()
    camera.shutdown()
    seq.shutdown()
    

if __name__ == "__main__":
    testSeqID()
