""" Defines a generic exception class. """


class Error(Exception):
        """
        """
        message = "Something bad happened.\n"











def test():
	try:
		raise Error("this is the message")
	except Exception, error:
		print "got an error!"
		print error.message
		print error.args
		




if __name__ == "__main__":
	test()
