"""
"""
import sys,os, LockFile

import Otis, SPLogger

from Fnorb.orb import BOA, CORBA   # use fnorb
import OtisCORBA_skel              # import the stubs


MYNAME = "ispfilterchanger"

class FilterChanger(OtisCORBA_skel.FilterChanger_skel):
    """
    """
    myName = MYNAME
    
    def __init__(self, boa):
        """
        """
        self.boa = boa

    def isAlive(self):
        """
        """
        return 1

    def getName(self):
        """
        """
        return self.myName

    def shutdown(self):
        """
        """
        self.boa._fnorb_quit()

    def setFilter(self, filterName, monitor):
        """
        """
        monitor.setRegistered()
        import time
        print "nap for 10"
        time.sleep(10)
        monitor.setDone()

    def getStatus(self):
        """
        """
        return -1

if __name__ == "__main__":
    
    lockFileName = "/home/skyprobe/ispFilterChanger.lock"
    iorFileName = "/home/skyprobe/ispFilterChanger.ior"

    
    try:
        lockFile = LockFile.LockFile(lockFileName)
    except LockFile.CantLock:
        print sys.argv[0],"already running with pid",lockFile.readline()
        os._exit(1)
	
     
    # Initialize the orb
    orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
    boa = BOA.BOA_init(sys.argv, BOA.BOA_ID)
    
    # Instantiate the server
        
    obj = boa.create(MYNAME, FilterChanger._FNORB_ID)
    impl = FilterChanger(boa)
    boa.obj_is_ready(obj, impl)
    # Get the IOR string
    iorString = orb.object_to_string(obj)
    
    # write out the IOR to a file just in case
    iorFile = open(iorFileName, "w")
    iorFile.write(iorString)
    iorFile.close()
    
    # register with the name server
    Otis.registerIOR(impl.getName(),iorString)
    
    
    print "isp filter changer up"
    #SPLogger.entry("Aloha from the Imaging SkyProbe!", 3)
    
    
    #orb.run()
    try:
        boa._fnorb_mainloop()
    except KeyboardInterrupt:
        print "got a cont-c!"
        impl.shutdown()
        
	
    # remove entry from name server
    Otis.removeIOR(impl.getName())
    
    
    print "isp filter changer is down"
    #SPLogger.entry("Imaging SkyProbe server has shutdown.", 3)
    
	
    # unlock the lockfile
    lockFile.close()
    os.unlink(iorFileName)
