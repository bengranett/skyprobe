import sys, os
import ORBit,CORBA
import Otis


ORBit.load_file("OtisCORBA.idl")
import OtisCORBA

class Log:
    """
    write to the log server
    If a connection cannot be made, write to stderr
    """
    timeout = 10
    
    def __init__(self):
        """
        initialize the log server
        """
        self.serverName= "BensTestServer"
        
        self.connect()

    def shutdown(self):
        self.logServer.shutdown()
        
    
    def connect(self):
        """
        connect to the log server if necessary
        """
        try:
                self.logServer = Otis.getServer(self.serverName, OtisCORBA.Log, self.timeout)
        except:
                raise
            
    def entry(self, mesg, level = 1):
        """
        record an entry
        """
        self.logServer.entry(self.serverName, level, mesg)
        


    def error(self, mesg, level = 1):
        """
        record an error
        """
        self.logServer.error(self.system, level, mesg)


        
    
        
def test():
    """
    """
    myLog = Log()

    myLog.entry("hello")


    
    mylog.logServer.shutdown()


    
if __name__ == "__main__":
    test()
