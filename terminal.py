#!/usr/local/bin/python
"""A class to communicate with serial devices.  This wraps the built in serial library for convenience.
"""

import sys, time
import serial


nan = float("nan")

class terminal:
    """A class to communicate with serial devices.  This wraps the built in serial library for convenience.
    """

    sleepTime = 10   # default sleep time in msec
    newLine = "\r"   # use this character to terminate lines.
    
    def __init__(self, dev):
        """Initialize the class.  You must specify the device, eg /dev/ttyUSB0"""
        self.dev = dev
        self.open()
    
    def pause(self, t=5):
        """Sleep for n thousandths of a second"""
        time.sleep(t/1000.)

    
    def open(self):
        """Open a connection to the device."""
        try:
            self.serial = serial.Serial(self.dev, timeout=1)
        except serial.serialutil.SerialException:
            raise NoConnectionError("Could not open port" + self.dev)


    def close(self):
        """Close the connection."""
        self.serial.close()


    def __write(self, string):
        """Send a string to the device."""
        try:
            self.serial.write(string)
            return
        except:
            # just try to open the port again
            pass

        self.pause(self.sleepTime)

        try:
            self.open()
            self.serial.write(string)
            return
        except:
            # oh well reraise the exception
            raise


    def __read(self):
        """Read a line from the device."""
        try:
            ans = self.serial.readline(eol=self.newLine);
            return ans
        except:
            pass

        self.pause(self.sleepTime)

        try:
            self.open()
            self.serial.readline()
            return
        except:
            # reraise the exception loudly
            raise

    def sendCmd(self, cmd, reply=False, intType = False, trial = 0):
        """Send a string command to the device and check for a reply.
        If the reply flag is set, a reply from the device is read and returned.
        If intType is set, the reply is checked to make sure it looks like an integer.
        The command will be sent again until a proper response is received or 10 trials
        are made.  This was added because the pontech board would sometimes spew giberish.
        Note that the reply is always returned as a string.  All command strings are ended
        with the newline character.
        """
        cmd = cmd.rstrip() + self.newLine

        #if len(cmd) > 20:
        #    print >>sys.stderr, cmd,"\n longer than 20 characters\n"
        #print "sending:",cmd

        try:
            self.__write(cmd)
        except:
            raise

        self.pause(3)

        if not reply: return

        try:
            ans = self.__read()
        except:
            raise # lets nurture this exception

        # Make sure that if we wanted an int,
        # we got something that looks like an int.
        if intType:
            try:
                int(ans)
                return ans
            except ValueError:
                print "got a bad response:",ans,trial
                trial += 1
                if trial > 10:
                    raise BadResponse(cmd,ans, trial)
                ans = self.sendCmd(cmd, reply = 1, intType = 1, trial = trial)
        return ans

#****************************************************************
#
# Error definitions
#
#****************************************************************
class NoConnectionError(Exception):
    """An exception to raise when an error occurs."""
    pass

class BadResponse(Exception):
    """The reply is not in the form that we want."""
    def __init__(self, cmd, ans, attempts):
        self.message = "Could not get valid response from motor.  \nCmd: %s\nans: %s\nattempts: %i\n"%(cmd, ans, attempts)
    def __str__(self):
        return self.message


#****************************************************************
#
# Test code
#
#****************************************************************
def test():
    dev = "/dev/ttyUSB1"
    t = terminal(dev)
    t.open()

    print "Connection made to ",t.dev
    print "----------"

    try:
        while True:
            sys.stdout.write(":-) ")
            line =  sys.stdin.readline()
            sys.stdout.write("-->"+t.sendCmd(line, 1)+"\n")
    except:
        pass

    print "good night"
    t.close()
          

if __name__ == "__main__":
    test()
