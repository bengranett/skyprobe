import MySQLdb

db = MySQLdb.connect(host="oobleck",      \
		     user="skyprobe",     \
                     passwd="skyprobe", \
                     db="otis")

c = db.cursor()

a = ("3","2","star3",15.0)
b = ("4","2","star4",16)

c.executemany("INSERT INTO sky_probe_mag (id, seq, star, mag) VALUES (%s, %s, %s, %s)", [a,b])

c.execute("select * from sky_probe_mag")
c.fetchall()

