#!/usr/local/bin/python

import sys, time, Utilities
import terminal
import Log

nan = float("nan")

class _superlogics:
    """Speak to a superlogics temperature probe controller."""

    ERROR = -1
    IDLE = 0
    
    defaults = {'SuperlogicsDev': "/dev/ttyUSB0",
                'mAddress': '01'
                }
    status = 0

    def __init__(self, config=None):
        """Open a serial connection to the device
        """
        if config: Utilities.loadConfig(self.defaults, config)
        self.term = terminal.terminal(self.defaults['SuperlogicsDev'])
        try:
            firmware = self.readFirmware()
        except NoConnection:
            self.status = -1
            Log.error("Cannot connect with Superlogics temperature probe.")
            raise NoConnection
    
    def close(self):
        """
        """
        self.term.close()

    def checkSum(self, string):
        """ Calculate the check sum
        """
        sum = 0
        string = string.rstrip()
        for i in range(len(string)):
            char = string[i:i+1]
            sum += ord(char)
        return hex(sum & 0x0FF)
             
    def readTemp(self):
        """ Return a tuple of measurements"""
        t = []
        cmd = '#'+self.defaults['mAddress']
        
        string = self.term.sendCmd(cmd, reply=1)
        if string == '':
            self.status = self.ERROR
            return [nan]*3
        self.status = self.IDLE

        string = string[1:]

        for i in range(3):
            if string[0:5] == "-0000":
                t.append(nan)
                string = string[5:]
                continue
            #print string[1:]
            t.append(float(string[0:7]))
            string = string[7:]
        return t

    def readFirmware(self):
        """ Return the firmware version to check that we're alive"""
        cmd = '$'+self.defaults['mAddress']+'F'
        string = self.term.sendCmd(cmd, reply=1)
        if string == '': raise NoConnection
        return string
        

class NoConnection(Exception):
    pass


_inst = None
def superlogics():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _superlogics()
    return _inst




def test():
    a = superlogics()    
    print "firmware:",a.readFirmware()
    print "current temp:",a.readTemp()
    a.close()
          

if __name__ == "__main__":
    test()
