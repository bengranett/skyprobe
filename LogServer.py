#!/usr/local/bin/python
"""
SkyProbe server

Ben Granett
"""

import sys, os
import ORBit,CORBA
import Otis

ORBit.load_file('OtisCORBA.idl')
import OtisCORBA__POA


class Log(OtisCORBA__POA.Log):
    """
    my log server
    """

    ALIVE = 1
    READY = 0
    ERROR = -1
    
    myName = "BensLog"
    
    def __init__(self):
        """
        initialize server instance
        """
        
        
    
    ###############################
    # Generic server routines
    ###############################
    def getName(self):
        """
        return the server name
        """
        return self.myName

    def isAlive(self):
        """
        Am I alive?
        """
        return self.ALIVE
        
    def shutdown(self):
        """
        shutdown the server
        """
        global orb
        # shutdown(1) does not work...?
        # 0 return immediately
        # 1 block until processing is complete, then return
        orb.shutdown(0)
        
        
    ###############################
    # SkyProbe specific routines
    ###############################
    def entry(self, subsystem, level, message):
        """
        record a log entry
        """
       	print "MESG>",subsystem, level, message
       	pass

    def error(self, subsystem, level, message):
        """
        record an error in the log
        """
        print "ERRR>", subsystem, level, message
        pass

    def save(self):
        """
        Save the log to disk
        """
        pass

    
if __name__ == "__main__":
	 
	
	# Initialize the orb
	orb = CORBA.ORB_init(sys.argv, "")
	
	# Instantiate the server
	servant = Log()
	objref = servant._this()
	
	# Get the IOR string
	iorString = orb.object_to_string(objref)
	
	# register with the name server
	Otis.registerIOR(servant.getName(),iorString)
	
	
	poa = orb.resolve_initial_references("RootPOA")
	poa._get_the_POAManager().activate()
	
	print "Bens log server is up"
	
	orb.run()
	
	# remove entry from name server
	Otis.removeIOR(servant.getName())
	
	
	print "Bens log server is down"
