import time as Time





def time():
    """returns a tuple (seconds, nanosec, leapsecond)"""
    now = Time.time()
    sec = int(now)   # seconds since 1970
    nanosec = int((now - sec) * 1e9)  # nanosec since last second
    return (sec, nanosec, 0)



def test():
    print time()


if __name__ == "__main__":
    test()
