"""
"""
import sys
from omniORB import CORBA, PortableServer
import OtisCORBA__POA
import OtisCORBA

class _Monitor(OtisCORBA__POA.Monitor):
    """
    """
    done = False

    def __init__(self):
        self.done = False
    
    def setRegistered(self):
        """
        """
        self.done = 0
        print " [monitor registered]"
        return

    def setDone(self):
        """
        """
        print ""
        print " [job finished]"
        self.done = 1
        return

    def setError(self, mesg):
        """
        """
        print ""
        print " [job died]", mesg
        self.done = 1
        return

    def isDone():
        if self.done: return True
        return False


orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")
poa._get_the_POAManager().activate()

def Monitor():
    monitorInst = _Monitor()
    monitorInst.obj = monitorInst._this()

    return monitorInst
