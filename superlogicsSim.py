#!/usr/local/bin/python

import sys, time, Utilities
#import Log
from math import *

nan = float("nan")

class _superlogics:
    """Speak to a superlogics temperature probe controller."""

    ERROR = -1
    IDLE = 0
    
    defaults = {'SuperlogicsDev': "/dev/ttyUSB0",
                'mAddress': '01'
                }
    status = 0

    def __init__(self, config=None):
        """Open a serial connection to the device
        """
        if config: Utilities.loadConfig(self.defaults, config)
        
    
    def close(self):
        """
        """
        pass

    def checkSum(self, string):
        """ Calculate the check sum
        """
        sum = 0
        string = string.rstrip()
        for i in range(len(string)):
            char = string[i:i+1]
            sum += ord(char)
        return hex(sum & 0x0FF)
             
    def readTemp(self):
        """ Return a tuple of measurements"""
        t = time.time()
        
        self.status = self.IDLE
	
	y1 = 5*sin(t*pi)*cos(t*pi/10)+20
	y2 = 4*sin(t*pi)*cos(t*pi/10+pi/2)+15
	y3 = 3*sin(t*pi)*cos(t*pi/10+pi)+10

	
        return (y1, y2, y3)

    def readFirmware(self):
        """ Return the firmware version to check that we're alive"""
	return "sim"
        

class NoConnection(Exception):
    pass


_inst = None
def superlogics():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _superlogics()
    return _inst




def test():
    a = superlogics()    
    print "firmware:",a.readFirmware()
    print "current temp:",a.readTemp()
    a.close()
          

if __name__ == "__main__":
    test()
