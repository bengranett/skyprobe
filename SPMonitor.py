"""Manage monitor objects
"""

import Log
log = Log.Log()

from omniORB import CORBA

class MonitorMan:
    """
    """
        
    def __init__(self, monitor=None):
        self.monitors = []
        self.locks = []
        
        if monitor:
            self.monitors.append(monitor)


    def addMonitor(self, monitor):
        if monitor:
            self.monitors.append(monitor)

    def addUnLock(self, unlocker):
        self.locks.append(unlocker)

    def setRegistered(self):
        for m in self.monitors:
            try:
                if m: m.setRegistered()
            except CORBA.SystemException, ex:
                
                log.error("system exception")
                print ex
                #print ex.id


            except:
                log.error("Can't contact monitor to register!")
                
                

    def setDone(self):
        for unlock in self.locks:
            unlock()
        for m in self.monitors:
            try:
                if m: m.setDone()
            except:
                log.error("Can't contact monitor, but I'm done!")

    def setError(self, mesg):
        for unlock in self.locks:
            unlock()
        for m in self.monitors:
            try:
                if m: m.setError(mesg)
            except:
                log.error("Can't contact monitor to report error: "+mesg)
