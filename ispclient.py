#!/usr/local/bin/python
"""
SkyProbe client

Ben Granett
"""

import sys, os, time, readline
from omniORB import CORBA
import OtisCORBA
import Otis
import Monitor

def help():
    """print a help message
    """
    print "------------------------------"
    print "*  Imaging Sky Probe client  *"
    print "------------------------------"
    print ""
    print "ISP commands"
    print ""
    print "getStatus    Print ISP status structure."
    print "go           Execute the current exposure sequence."
    print "setSeq       Input an exposure sequence."
    print "setTarget t  Enter the target type. (Dome, sky or twilight.)"
    print "setreadout m set the readout mode (fast12, slow12 or slow16)"
    
    print "setFilter f  Change the current filter. (g,r,i,z,y or 'dark','same'...)"
    print "setFilterAngle d   Set the angle of the filter wheel"
    print "homeFilter   Home the filter wheel."
    print "setFocus n   Set the focus to position n."
    print "homeFocus    Home the focus ring."
    print "abort        Stop current activities."
    print "powerdown    turn off skyprobe"
    
    print ""
    print "Server utilities:"
    print ""
    print "getName       print the name of the corba server."
    print "isAlive       ping the corba server."
    print "shutdown      shutdown the corba server."

def isAlive():
    if sp.isAlive():
        print "True"
        return
    print "False"

def getName():
    print sp.getName()

def shutdown():
    print "Warning! shutting down the skyprobe server!"
    sp.shutdown()
    print "client going down too... ciao"
    sys.exit(0)

def getStatus():
    """"""
    print "status:"
    print "------------------------"
    status= sp.getStatus()
    print status.state.details
    print "time:   ",status.state.time.seconds, status.state.time.nanosec, status.state.time.leapsec
    print "focus:  ",status.focus
    print "focus_homed:",status.focus_homed
    print "filter: ",status.filter
    print "angle:  ",status.filter_position
    print "filter homed:", status.filter_homed
    print "type:   ",status.target_type
    print "automof:",status.auto_mode
    print "duratio:",status.exposure
    print "elapsed:",status.elapsed
    print "status: ",status.state.text,"Color:", status.state.color
    for temp in status.temperatures:
        print temp.name, temp.celsius
    i=0
    print "seqid:",status.id
    print "Loaded sequence:"
    for exp in status.seq:
        i+=1
        print "  exp "+str(i)+":",exp.duration, exp.filter, exp.focus
    print "read out mode:", status.readout
    print "CCD temp:", status.ccd_temperature
    print "CCD Cooler:", status.cooler_on
    print "CCD setpoint:", status.cooler_set_point
    print "CCD cool drive:", status.cooler_drive


def setReadOut(mode):
    m = None
    if mode == "fast12": m = OtisCORBA.SkyProbe.FAST12
    if mode == "slow12": m = OtisCORBA.SkyProbe.SLOW12
    if mode == "slow16": m = OtisCORBA.SkyProbe.SLOW16
    if not m:
        print "invalid read out mode"
        return
    sp.setReadoutMode(m)

def setTarget(type):
    targ = None
    if type == "dome":
        targ = OtisCORBA.DOME
    if type == "sky":
        targ = OtisCORBA.SKY
    if type == "twilight":
        targ = OtisCORBA.TWILIGHT
    if targ == None:
        print "invalid target type"
        return
    sp.setTargetType(targ)

def setFilter(f):
    monitors.append(Monitor.Monitor())
    try:
        sp.setFilter(f, monitors[len(monitors)-1].obj)
    except OtisCORBA.BadCommandException:
        print OtisCORBA.BadCommandException,"Exception received.  prolly, invalid filter name."

def setFilterAngle(d):
    monitors.append(Monitor.Monitor())
    try:
        sp.setFilterAngle(d, monitors[len(monitors)-1].obj)
    except OtisCORBA.BadCommandException:
        print OtisCORBA.BadCommandException,"Exception received."

    
def setFocus(n):
    monitors.append(Monitor.Monitor())
    sp.setFocus(n, monitors[len(monitors)-1].obj)

def homeFocus():
    monitors.append(Monitor.Monitor())
    sp.homeFocus(monitors[len(monitors)-1].obj)

def homeFilter():
    monitors.append(Monitor.Monitor())
    sp.homeFilter(monitors[len(monitors)-1].obj)
    
def setSequence():
    seq=[]
    print "Enter one exposure per line: filter expt focus (ntimes)"
    print "Leave line blank to stop"
    n = 0
    while True:
        n+=1
        sys.stdout.write("exp "+str(n)+": ")
        #line = sys.stdin.readline()
        line = raw_input()
        line = line.strip()
        if line == "": break
        list = line.split()
        (f, expt_s, foc_s) = list[:3]
        ntimes = 1
        if len(list) == 4:
            ntimes = float(list[-1])
        expt = float(expt_s)
        foc = int(foc_s)
        for i in range(ntimes):
            seq.append(OtisCORBA.SkyProbe.Exposure(f, expt, foc))
    print "Here is your exposure sequence:"
    n = 0
    print "n filter duration focus"
    for exp in seq:
        n+=1
        print n, exp.filter, exp.duration, exp.focus
    try:
        sp.setSequence(seq)
    except OtisCORBA.BadCommandException:
        print "invalid sequence"

def setIDBase(base):
    sp.setIDBase(base)

    
def setID(id):
    sp.setID(id)

def go():
    monitors.append(Monitor.Monitor())
    try:
        sp.go(monitors[len(monitors)-1].obj)
    except OtisCORBA.BadCommandException, error:
        print error

def coolerOn():
    sp.setCCDCooler(True)
    
def coolerOff():
    sp.setCCDCooler(False)

def setCCDTemp(temp):
    sp.setCCDThermostat(temp)

def gonm():
    sp.go("0",None)

def abort():
    sp.abort()

def powerdown():
    shutdown() 

def parse(cmd):
    """
    """
    line = cmd.strip().lower()
    if line == "": return
    if line[0] == "#": return
    words = line.split()
    name = words[0]
    if name == "demo":
        demo()
        return
    if name == "settemp":
        setCCDTemp(float(words[1]))
        return
    if name == "coolon":
        coolerOn()
        return
    if name == "cooloff":
        coolerOff()
        return
    if name == "isalive":
        isAlive()
        return
    if name == "getname":
        getName()
        return
    if name == "shutdown":
        shutdown()
        return
    if name == "getstatus":
        getStatus()
        return
    if name == "setfilter":
        setFilter(words[1])
        return
    if name == "setfilterangle":
        setFilterAngle(float(words[1]))
        return
    if name == "setfocus":
        setFocus(int(words[1]))
        return
    if name == "setseq":
        setSequence()
        return
    if name == "setid":
        setID(words[1])
        return
    if name == "setbase":
        setIDBase(words[1])
        return
    if name == "go":
        go()
        return
    if name == "gonm":
        gonm()
        return
    if name == "abort":
        abort()
        return
    if name == "powerdown":
        powerdown()
        return
    if name == "settarget":
        setTarget(words[1])
        return
    if name == "setreadout":
        setReadOut(words[1])
        return
    if name == "homefilter":
        homeFilter()
        return
    if name == "homefocus":
        homeFocus()
        return
    if name == "wait":
        wait()
        return
    if name[0] == "?" or name[0] == "h":
        help()
        return
    
    print "???"


def cleanMonitors():
    """Delete used monitors
    """
    i=0
    while i < len(monitors):
        if monitors[i].done:
            monitors.pop(i)
        else:
            i+=1

def wait():
    """ Wait for all jobs to finish
    """
    while len(monitors) > 0:
        cleanMonitors()
        time.sleep(1)

def demo():
    """
    """
    filters = ['g','r','i','z','y']
    j=0
    while True:
        for f in filters:
            setFilter(f)
            if j==0:
                j=237
            else:
                j=0            
            setFocus(j)
            wait()


monitors = []

if __name__ == "__main__":
    try:
        sp = Otis.getServer("ImSkyProbe", 5)
    except Otis.NoServer:
        print "server is not up"
        sys.exit(1)

    help()
    
    try:
        while True:
            #sys.stdout.write(":) ")
            #input=sys.stdin.readline()
            input = raw_input(":) ")
            #if input == "": break
            parse(input)
            cleanMonitors()
    except EOFError:
        pass
    except KeyboardInterrupt:
        pass

    if len(monitors)>0:
        print "waiting for jobs to finish."
        wait()
        
    
    print ""
    print "good morning"

