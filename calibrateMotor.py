#!/usr/local/bin/python

import time
import motor
import scipy

class CalMotor(motor.Motor):
	"""Measure stepper motor speed
	"""
	
	def go(self):
		"""
		"""
		setPowerMode(self.POWERON)
		setStepMode(self.FULLSTEP)
	 	
		numRev = 4.
		nsteps = 360.*numRev/self.stepSize

		delays = [1200,1400,1600,1800,2000,3000]
		freq = []

		for delay in delays:
			setDelay(delay)
			tstart = time.time()
			setRelPos(theta)			
			waitWhileMoving()
			tend = time.time()
			t = 360*(tend - tstart)/numRev
			freq.append(t)
			
		fit = scipy.stats.linregress(delays, freq)
		A = fit[0]
		b = fit[1]
		print fit

def test():
	"""
	"""
	m = calMotor(0)

if __name__ == "__main__":
	test()
