"""SkyProbe configuration

All configuration parameters are stored in the config
dictionary and indexed via string keys.  The settings
in this file will overwrite any defaults that are specified
in the Sky Probe code.

To use this file, do

>> from SkyProbeConf import config

for instance at the top of the source file.  Then
parameters can be accessed easily like this:

>> myName = config['MyName']

"""

import constants

#
# All parameters are stored in this dictionary named
# config.  Parameters are indexed with a string key
#
config = {}

#
# A version number, for kicks
#
config['ISPversion'] = 0.1

#
# Simulation mode, on or off
#
config['SimSkyProbe'] = False

#
# Turn extra debug info on
#
config['debug'] = False

#
# The server name to be used with CORBA
#
config['CorbaConfigServer'] = ('', 1050)
config['CorbaConfigServerSim'] = ('', 1050)
config['MyName'] = "ImSkyProbe"
config['CorbaLogServer'] = "Log"

#
# where the server lives
#
config['HomeDir'] = "/home/skyprobe/"

#
# the lock file to use
#
config['LockFileName'] = config['HomeDir']+"SkyProbe.lock"

#
# the file to store the ior string
#
config['IORFileName'] = config['HomeDir']+"SkyProbe.ior"

#
# output files to use in daemon mode
#
config['StdOutFile'] = "/www/log/SkyProbe.log"
config['StdErrFile'] = "/www/log/SkyProbe.log"


#
# file to store sequence history
# Stored in this file is a pickled python dictionary with a record
# of past base IDs.
#
config['seqHistFile'] = config['HomeDir']+"ispsequence.history"
#
# The format string used to make a sequence ID from a base string.
#
config['seqExtFormatStr'] = "_%03i"
config['fileNameFormatStr'] = "%s_%02i.fits"



#
# startCmd
#
config['StartCmd'] = "/usr/local/bin/python /home/skyprobe/skyprobe/src/ImSkyProbeServer.py -d\n"


#
# Messages
#
config['TextIdle'] = "Idle"
config['TextExposing'] = "Exposing"
config['TextChanging'] = "Changing"
config['TextReading'] = "Reading image"
config['TextCameraError'] = "Camera alert"
config['TextFilterError'] = "Filter alert"
config['TextFocusError'] = "Focus alert"
config['TextSubsysError'] = "Subsys alert"
config['TextHoming'] = "Homing"
config['TextHomeFocus'] = "Home Focus"
config['TextHomeFilter'] = "Home Filter"

#
# Filter Wheel
#
config['FilterNum'] = 5
config['FilterLookup'] = {'g':0,'r':1,'i':2,'z':3,'y':4}
config['FilterReverseLookup'] =["g", "r", "i", "z", "y"]
config['FilterPositions'] = {"g":0, "r":3818, "i":7680, "z":11523, "y":15350}
config['FilterCodes'] = {'NONE': '', 'SAME': 'same', 'DARK': 'dark'}
config['FilterPollTime'] = 0.5       # poll position every half second

#
# Apogee camera
#
config['CCDApogeeLib'] = "/opt/apogee/lib/"
config['CCDFitsHeader'] = "Default.hdr"
config['CCDBiasName'] = "bias.fits"
config['CCDDarkName'] = "dark.fits"
config['CCDSnapName'] = "snap.fits"
config['CCDDefaultImageName'] = "expose.fits"

config['CCDExposureType'] = {"BIAS": "BIAS",
                             "DARK": "DARK",
                             "LIGHT": "LIGHT",
                             "FLAT": "FLAT",
                             "TEST": "TEST"}



config['CCDImagingRows'] = 2048
config['CCDImagingCols'] = 2048
config['CCDDigitizeOverscan'] = 1 
config['CCDOverscanCols'] = 50
config['CCDCoolerTimeOut'] = 3600
config['CCDBinMode'] = 1
config['CCDDepth'] = 16

#
# Pontech stepper motor configuration
#
config['FocusPollTime'] = 0.5            # poll position every half second
config['PontechDev'] = "/dev/ttyUSB2"    # Device the motor is plugged in to.
config['PontechLowLimit'] = 5            # The pin that the low limit switch is connected to.
config['PontechHighLimit'] = 6           # The pin that the high limit switch is connected to.
config['PontechStepDelay'] = 10000       # The step delay used to control the speed of the motor
config['PontechStepAccel'] = 0           # The step acceleration
config['PontechStepMin'] = 0             # The minimum step something to do with acceleration

#
# Animatics motor configuration
#
config['AniDev'] = "/dev/ttyUSB1"
config['AniHomeVelocity'] = -10000
config['AniMoveTimeOut'] = 60
config['AniVelocity'] = 10000
config['AniAcceleration'] = 2
config['AniBrakeMode'] = "BRKTRJ"
config['AniFilterPos'] = {"g":0, "r":3818, "i":7680, "z":11523, "y":15350}
config['AniTune'] = {'KD':1100, 'KP':150, 'KI':11, 'KA':2, 'KL':100}
config['AniFullCircle'] = 19200

#
# Superlogics temperature probe parameters
#
config['SuperlogicsDev'] = "/dev/ttyUSB0"
config['TempProbeNames'] = ("Power supplies", "Compuper", "ISP ambient")


#
# start up default sequence -- this will run if you send 'go' without programming
# a sequence first.
#
config['SequencerDefault'] = [constants.Exposure("dark",1,-1)]

#
# mysql
#
config['MYSQLHost'] = ""
config['MYSQLHostSim'] = ""
config['MYSQLUser'] = ""
config['MYSQLPassword'] = ""
config['MYSQLDB'] = ""

config['MYSQLHKLabels'] = ("time","cooler_set_point","cooler_drive", "cooler_status","isp_status","ccd_status")
config['MYSQLTempLabels'] = ("time","sensor","temperature")
config['MYSQLExpoLabels'] = ("id","seq","time","nanosec","leapsec","exposure","filter","type","focus")
config['MYSQLSeqLabels'] = ("id","base","ext")

config['MYSQLHKTable'] = "sky_probe_hk"
config['MYSQLSeqTable'] = "sky_probe_sequence"
config['MYSQLExposeTable'] = "sky_probe_exposure"
config['MYSQLTemperatureTable'] = "sky_probe_temperature"

config['HeartBeatSleep'] = 30
