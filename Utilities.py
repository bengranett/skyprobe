"""Functions to use all over the place."""



def loadConfig(defaults, newConfig):
        """
        Load configuration values from an input config dictionary.  If the
        dictionary contains keys that are in the local defaults dict, the
        defaults will be overwritten.  No sanity checks are done, so double
        check the values you are passing in.
        """
        for key in defaults.keys():
            if newConfig.has_key(key):
                defaults[key] = newConfig[key]


def test():
	"""
	"""
	UserConfig = {'MyName': "Ben"}
	defaults = {'MyName': "George"}
	loadConfig(defaults, UserConfig)

	assert(defaults['MyName'] == "Ben")
	print "passed"

if __name__ == "__main__":
	test()
