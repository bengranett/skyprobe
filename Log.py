#!/usr/local/bin/python
"""
A "singleton" class to access the log server.

If a connection cannot be made, stderr is written to.

Usage:
import Log
log = Log.Log()
log.entry("message", level = 1)
log.error("message", level = 1)

"""

import sys, traceback, time
from omniORB import CORBA
import OtisCORBA
import Otis
from SkyProbeConf import config

system = config['MyName']

class _Log:
    """
    write to the log server
    If a connection cannot be made, write to stderr

    """
    timeout = 1
    failureLimit = 1
    tryAgainAfter = 600

    status = {'logWorking' : False,
              'numFailures' : 0,
              'lastTime' : -1}
    
    def __init__(self, system, verb = 1):
        """
        initialize the log server
        """
        self.verb = verb
        
        self.serverName= config['CorbaLogServer']
        
        self.system = system
        
        self.ready = 0
        try:
            self.connect()
        except:
            pass
            
    def connect(self):
        """
        connect to the log server if necessary
        """
        if not self.status['logWorking']:
            timeLeft = self.status['lastTime'] - time.time() + self.tryAgainAfter
            if timeLeft > 0:
                print >>sys.stderr, "ALERT!!! log server %s is down, i'll try again in %i seconds."%(self.serverName, timeLeft)
                raise LogIsDown
            self.status['numFailures'] = 0
            self.status['lastTime'] = time.time()
        
        try:
            self.logServer = Otis.getServer(self.serverName, self.timeout)
            self.status['logWorking'] = True
            self.status['numFailures'] = 0
            self.status['lastTime'] = time.time()
        except:
            self.status['numFailures'] += 1
            if self.status['numFailures'] > self.failureLimit:
                self.status['logWorking'] = False
            raise
            
    def entry(self, mesg, level = 1):
        """
        record an entry
        Levels:
        3 - Something which happens rarely, e.g. once a day,
            like subsystem startup or shutdown.
        2 - Something which indicates the start of a normal
            operating cycle, e.g. the start of an exposure,
        1 - detailed events within an operating cycle
        0 - Debug string.  Don't write to corba server.

        """
        # get the filename, call and line number
        tb = traceback.extract_stack()
        tb.pop()
        tb.pop()
        trace = tb.pop()
        
        file = trace[0].split("/")[-1]   # Don't include the full path
        line = trace[1]
        call = trace[2]

        mesg = "["+file+" "+call+" "+str(line)+"] "+mesg
        
        if level == 0:
            print >>sys.stderr, "MESSG:", self.system, level, mesg
            return
            
        try:
            self.logServer.entry(self.system, level, mesg, "")
            if self.verb: print >>sys.stderr, "MESSG:", self.system, level, mesg
            return
        except:
            pass
        
        
        try:
            self.connect()
            self.logServer.entry(self.system, level, mesg, "")
            if self.verb: print >>sys.stderr, "MESSG:", self.system, level, mesg
            return
        except LogIsDown:
            pass
        except CORBA.Exception:
            print >>sys.stderr, "CORBA exception while connecting to", self.serverName
        except Otis.Error:
            print >>sys.stderr, "Otis exception while connecting to", self.serverName
        except:
            print >>sys.stderr, "Unknown exception caught while trying to write to", self.serverName
            raise
        print >>sys.stderr, "MESSG:", self.system, level, mesg
    
    
    
    def error(self, mesg, level = 1):
        """
        record an error
        "level" has the following meaning:
        3 - A fatal error
        2 - An error which should be immediately brought to the
            attention of a human. A level 2 error results in
            someone getting email or being paged.
        1 - A minor error, which should be logged, but for which
            it's not worth waking anybody up
        0 - Debug string.  Don't send to corba server.
       
        """
        # get the filename, call and line number
        tb = traceback.extract_stack()
        tb.pop()
        trace = tb.pop()
        trace = tb.pop()
        file = trace[0].split("/")[-1]
        line = trace[1]
        call = trace[2]

        mesg = "["+file+" "+call+" "+str(line)+"] "+mesg
        
        if level == 0:
            print >>sys.stderr, "ERROR:", self.system, level, mesg
            return
        
        try:
            self.logServer.error(self.system, level, mesg, "")
            if self.verb: print >>sys.stderr, "ERROR:", self.system, level, mesg
            return
        except:
            pass
        
        
        try:
            self.connect()
            self.logServer.error(self.system, level, mesg, "")
            if self.verb: print >>sys.stderr, "ERROR:", self.system, level, mesg
            return
        except LogIsDown:
            pass
        except CORBA.Exception:
            print "CORBA exception while connecting to", self.serverName
        except Otis.Error:
            print "Otis exception while connecting to", self.serverName
        except:
            print "Unknown exception caught while trying to write to", self.serverName
        print >>sys.stderr, "ERROR:", self.system, level, mesg


#------------------------------------ fake a singleton class
_inst = None
def Log():
    """Fake a singleton class

    This function returns a singleton instance of _Log.  If it has not been
    initialized, it creates a new instance.

    """
    global _inst
    if not _inst:
        _inst = _Log(system)
    return _inst
    
#------------------------------------------------------------



def entry(mesg, level = 1):
    """Gives an easy interface to the log class"""
    log = Log()
    log.entry(mesg, level)

def error(mesg, level = 1):
    """Gives an easy interface to the log class"""
    log = Log()
    log.error(mesg, level)


class LogIsDown(Exception):
    pass

    
def test():
    """
    """
    entry("Testing the log", 1)
    time.sleep(3)
    error("Testing the log", 1)
    
if __name__ == "__main__":
    test()
