""" threader.py

    A basic thread pool module.

    Ben Granett
"""
import sys
import threading, Queue
import Log, traceback



class Command:
    """A generic command class.

    Put your routine in execute and make sure it gets proper inputs, sets
    appropriate outputs and deals with errors.  execute() is called by the
    thread pool with no arguments.

    """
    doneEvent = None
    def __init__(self):
        pass
    
    def execute(self):
        """Overwrite this method with your own execute command.
        """
        # print "thread executing..."
        # print "this method should have been overwritten by you!"
        pass

    def init(self):
        """Clear the done flag.
        """
        if not self.doneEvent:
            self.doneEvent = threading.Event()
        self.doneEvent.clear()

    def done(self):
        """Called when execution is done to set the done flag.
        """
        self.doneEvent.set()

    def wait(self):
        """This blocks until the done flag is set.
        """
        return self.doneEvent.wait()

    def error(self, msg, level = 1):
        """Default error handling just write the error to the log.
        """
        info = sys.exc_info()
        msg+="\n"+str(info[0])+"\n"+str(info[1])
        Log.error(msg, level = level)



class worker(threading.Thread):
    """The worker thread class.
    
    A worker thread checks the global jobQueue and processes jobs as they come
    off the queue.
    
    """
    
    def __init__(self, jobQueue):
        """Initialize this thread.
        """
        # raise this event to kill mee
        self.die = threading.Event()

        # set when i'm doing a job
        self.busy = threading.Event()
        
        # check if we should die with period...
        self.waitTime = 0.5
        
        # make a copy of the thread pool job queue
        self.jobQueue = jobQueue

        # run the thread class initialization
        threading.Thread.__init__(self)

        # fire up this thread
        self.start()
        
     
    def run(self):
        """Death taxes and keep working.

        This run loop goes until the die event is received.  It continually
        grabs jobs off the pool job queue and executes them.
        
        """
        # begin an endless loop sitting on the job queue
        while not self.die.isSet():
            # it blocks until a job is received
            # Use a timeout so we can check if
            # we have been killed.  Timeout should be
            # relatively large.
            try:
                job = self.jobQueue.get(timeout = self.waitTime)
            except Queue.Empty:
                continue
            
            # if we have a job, execute it
            # we are going to let the job itself
            # deal with arguments, outputs and errors.
            self.busy.set()
            # start the job
            print "executing job"
            job.init()
            job.execute()
            # set the done flag
            job.done()
            self.busy.clear()

    def isBusy(self):
        """ Return if I am busy or not.
        """
        return self.busy.isSet()

    def kill(self):
        """ Let me die after i finish the current job.

        This routine sets the kill flag.

        """
        self.die.set()


class killCommand(Command):
    """Queueing this command up will kill a thread dead.
    """
    def __init__(self, killEvent):
        """Call it with the kill event you want called.
        """
        self.killEvent = killEvent
        Command.__init__(self)
        
    def execute(self):
        """Execute the kill event.
        """
        self.killEvent.set()


class threadPool:
     """The thread pool.
     """
     # we can count up how many threads are busy
     busyCount = 0

     # set if we are trying to shutdown.
     goingDown = False

     
     def __init__(self, numThreads):
         """Initialize it with some number of threads.
         """
         self.jobQueue = Queue.Queue()
         
         self.threads = []
         self.createThreads(numThreads)

     def createThreads(self, numThreads):
         """Create a number of threads.
         """
         for i in range(numThreads):
             self.threads.append(worker(self.jobQueue))

     def killWorkers(self, numKills, block = True):
         """Kill a number of worker threads.

         Arguments
         numKills -- number of threads to kill
         block    -- boolean, if True, block until the threads or dead.
         
         """
         blockList = []
         
         for i in range(min(numKills, len(self.threads))):
             worker = self.threads.pop()
             worker.kill()
             
             if block:
                 blockList.append(worker)

         for worker in blockList:
                 worker.join()
             

     def shutdown(self, wait = True, block = True):
         """Shutdown this pool.
         
         Arguments
         wait  -- wait for the queue to empty before shutting down
         block -- don't return until the job is done.

         """
         # if we are already shutting down, do nothing
         if self.goingDown: return
         
         # don't take anymore jobs
         self.goingDown = True
         
         if not wait:
             self.killWorkers(len(self.threads), block = block)
             return

         # if we want the job queue to empty,
         # then just append it with a bunch of
         # kill jobs.
         for worker in self.threads:
             kill = killCommand(worker.die)
             self.jobQueue.put(kill)

         # wait for all to die
         if block:
             for worker in self.threads:
                 worker.join()

         self.threads = []
        
         
     def poll(self):
         """Poll the threads and count how many are working.
         """
         count = 0
         for worker in self.threads:
             if worker.isBusy():
                 count += 1
         return count
         

     def getStatus(self):
         """Return status of the pool"""
         nBusy = self.poll()
         nTotal = len(self.threads)
         return (nBusy, nTotal)

     def do(self, job, block = False):
         """Give the pool a job to work on.
         
         If there are no threads in the pool, one lonely worker is created.

         Arguments
         job   -- the command instance to run.
         block -- boolean, if True, return when the job is done.
         
         """
         if not self.goingDown:
             if len(self.threads) == 0:
                 self.createThreads(1)

             job.init()
             self.jobQueue.put(job)
             print "job put on queue"
         else:
             # this should raise an exception!!!
             print "got a new job while trying to shutdown!!"
             pass

         # if we want, block until the job is completed.
         if block:
             job.wait()

         print "job is on the queue"

def test():
    """ Thread pool test routines.
    """
    import time
    
    print "running threader self test"
    
    class test(Command):
        def __init__(self, id):
            self.id = id
            
        def execute(self):
            print "I am running", self.id
            print "sleeping 5"
            time.sleep(5)
            print "i'm done",self.id

    job1 = test(1)
    job2 = test(2)
    job3 = test(3)

    pool = threadPool(2)

    pool.do(job1)
    time.sleep(1)
    print "num busy",pool.poll()
    pool.do(job2)
    print "STATUS",pool.getStatus()
    time.sleep(1)
    print "num busy",pool.poll()
    print "STATUS",pool.getStatus()
    pool.do(job3)
    
    time.sleep(1)
    print "num busy",pool.poll()
    pool.shutdown(wait = True, block = True)
    print "all done"
    print "STATUS",pool.getStatus()


if __name__ == "__main__":
    test()
