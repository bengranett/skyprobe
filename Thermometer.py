#!/usr/local/bin/python

import sys, time
import threader, threading
import Log
from SkyProbeConf import config
import constants as const

if SkyProbeConf['SimSkyProbe']:
    import superlogicsSim as superlogics
else:
    import superlogics



class _Thermometer:
    """
    """
    abortEvent = threading.Event()
    tempLock = threading.Lock()
 
    status = {'lastRead': -1,
              'temp': [const.nan]*3,
              'status': const.IDLE
              }
   
    class monitorTempCmd(threader.Command):
        """ A command class for threader to queue up.
        Read the temperature
        """
        def __init__(self, parent):
            self.parent = parent

        def execute(self):
            try:
                self.parent._monitorTemp()
            except:
                self.error("Exception caught while talking to superlogics.")
        
    def __init__(self):
        """
        """
        global config

        # do some sanity checks
        assert(len(config['TempProbeNames']) == 3)
        
        
        self.modulePresent = True
        
        try:
            self.module = superlogics.superlogics()
        except:
            Log.error("Cannot contact superlogics temperature module.", level=2)
            self.modulePresent = False
                    

        self.abortEvent.clear()

        # start the thread to poll the superlogics module
        self.ThermometerThreadPool = threader.threadPool(1)
        cmd = self.monitorTempCmd(self)
        self.ThermometerThreadPool.do(cmd, block=False)

    def shutdown(self):
        """
        """
        # stop the monitor
        self.abortEvent.set()
        # shutdown the pool
        self.ThermometerThreadPool.shutdown()

    def _monitorTemp(self):
        """ Monitor the temperature
        """
        while not self.abortEvent.isSet():
            if self.modulePresent:
                t = self.module.readTemp()

                self.tempLock.acquire()
                self.status['temp'] = t
                self.status['lastRead'] = time.time()
                self.tempLock.release()

            time.sleep(1)
        print "shutdown"
            
    def readTemp(self):
        """ Return the last measurement
        """
        self.tempLock.acquire()
        temp = self.status['temp']
        time = self.status['lastRead']
        self.tempLock.release()

	temperatures = {}
	for i in range(len(temp)):
	    temperatures[config['TempProbeNames'][i]] = temp[i]

        return temperatures, time



_inst = None
def Thermometer():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _Thermometer()
    return _inst




def test():
    t = Thermometer()
    tstart= time.time()
    while (time.time()-tstart) < 10:
        print t.readTemp()
        time.sleep(1)
    t.shutdown()
    

if __name__ == "__main__":
    test()
