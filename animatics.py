#!/usr/local/bin/python

""" animatics motor controller

to do:
--on move command, if we are already in position do nothing.  releasing and setting the brake
can move the wheel!
--add "shortest distance" algorithm
--make home command go forward 180 and then back 180 to find home, dont go 360.
"""



import sys, time
import terminal
import Utilities

class _animatics:
    """
    """
    # constants
    LOW = 0
    HIGH = 1
    
    POSITION_MODE = "MP"
    VELOCITY_MODE = "MV"
    TORQUE_MODE = "MT"

    ENGAGE_BRAKE = "BRKENG"     # dont do this ever
    RELEASE_BRAKE = "BRKRLS"
    BRAKE_IF_STOP = "BRKSRV"    # dont do this ever
    BRAKE_WHEN_STOP = "BRKTRJ"  # do this :)

    defaults = {'AniDev': "/dev/ttyUSB1",
                'AniHomeVelocity': -10000,
                'AniMoveTimeOut': 60,
                'AniVelocity': 10000,
                'AniSlooow': 1000,
                'AniAcceleration': 2,
                'AniBrakeMode': BRAKE_WHEN_STOP,
                'FilterReverseLookup': ["g", "r", "i", "z", "y"],
                'FilterPositions': {"g":0, "r":3818, "i":7680, "z":11523, "y":15350},
                'AniTune': {'KD':1100, 'KP':150, 'KI':11, 'KA':2, 'KL':100},
                'AniFullCircle': 19200
                }

    params = {"mode": POSITION_MODE,
              "brakeMode": BRAKE_WHEN_STOP,
              "velocity": 1000,
              "acceleration": 2,
              "position": 0
              }

    def __init__(self, config=None):
        """
        """
        # load an input configuration dictionary if supplied
        if config: Utilities.loadConfig(self.defaults, config)

        # open the serial port
        self.term = terminal.terminal(self.defaults['AniDev'])

        self.setParams()
               
    def setParams(self):
        """Set parameters to default state"""
        # Echo must be off
        self.term.sendCmd("ECHO_OFF", True)

        # These set the response, damping etc of the motor.
        tune = self.defaults['AniTune']
        self.term.sendCmd("KD=%i"%(tune['KD']))
        self.term.sendCmd("KP=%i"%(tune['KP']))
        self.term.sendCmd("KI=%i"%(tune['KI']))
        self.term.sendCmd("KA=%i"%(tune['KA']))
        self.term.sendCmd("KL=%i"%(tune['KL']))
        self.term.sendCmd("F")

        self.params['mode'] = self.POSITION_MODE
        self.params['brakeMode'] = self.defaults['AniBrakeMode']
        self.params['velocity'] = self.defaults['AniVelocity']
        self.params['acceleration'] = self.defaults['AniAcceleration']
        
        self.setBrake()

    def shutdown(self):
        """Close connection to the motor."""
        self.stop()
        self.term.close()

    def readTemperature(self):
        """Return the motor temperature sensor reading"""
        self.term.sendCmd("t=TEMP",False)
        return float(self.term.sendCmd("Rt",True))

    def countsToDeg(self, n):
        """Convert counts to degrees"""
        return n * 360. / float(self.defaults['AniFullCircle'])

    def degToCounts(self, d):
        """convert decimal degrees to counts"""
        return int(d * self.defaults['AniFullCircle'] / 360.)
    
    def readFirmware(self):
        """Return the firmware string."""
        
        return self.term.sendCmd("RSP", True)


    def setVelocity(self, v):
        """Set the velocity."""
        self.params["velocity"] = v
        
    def setAcceleration(self, a):
        """Set the acceleration."""
        self.params["acceleration"] = a


    def setPosition(self, n, slop=5):
        """Move the motor to a desired position."""
        delta = self.params['position'] - n
        if abs(delta) < slop:
            return
        
        self.params["position"] = n
        self.go()

    def setAngle(self, d):
        """Set the position in degrees."""
        self.setPosition(self.degToCounts(d))
        
    def releaseBrake(self):
        """release the brake!"""
        self.params["brakeMode"] = self.RELEASE_BRAKE
        self.term.sendCmd(self.RELEASE_BRAKE )
        
        
    def setBrake(self):
        """set the brake when stopped"""
        self.params["brakeMode"] = self.BRAKE_WHEN_STOP
        self.term.sendCmd(self.BRAKE_WHEN_STOP)
        pass

    def zeroEncoder(self):
        """label the current position 0
        """
        self.term.sendCmd("O=0")
        self.params["position"] = 0

    def powerOff(self):
        """"""
        self.term.sendCmd("OFF")

    def setRelative(self, n):
        """Move relative n counts"""
        self.params["position"]+=n

    def isMoving(self):
        """"""
        if int(self.term.sendCmd("RBt", True, intType=True)):
            return True
        return False

    def stop(self):
        """Stop the motor"""
        self.term.sendCmd("X")

    def waitWhileMoving(self):
        """"""
        start = time.time()
        while self.isMoving():
            if time.time() - start > self.defaults['AniMoveTimeOut']:
                print >>sys.stderr, "timed out waiting for the filter wheel to move..."
                self.stop()
                raise timeOut
            time.sleep(0.1)
        time.sleep(1.0)
            

    def home(self):
        """Home the encoder."""
        # if the switch is triggered, move away from it and rehome
        limit = int(self.term.sendCmd("UDI a=UDI Ra", True, intType=True))


        if limit == self.LOW:
            current = self.getPosition()
            self.setPosition(current + 200)
            self.waitWhileMoving()
        else:
            self.term.sendCmd("UDM")  # set input D as a left (minus) limit switch
            self.term.sendCmd("MV")   # put into velocity mode
            self.term.sendCmd("V=%i"%self.defaults['AniHomeVelocity']) # set the velocity
            self.term.sendCmd("G")    # start spinning
            self.waitWhileMoving()

            current = self.getPosition()
            print "first guess at home:", current

            self.setPosition(current + 200)
            self.waitWhileMoving()

        self.term.sendCmd("UDM")  # set input D as a left (minus) limit switch
        self.term.sendCmd("MV")   # put into velocity mode
        self.term.sendCmd("V=%i"%(self.defaults['AniHomeVelocity']/20.)) # set the velocity
        self.term.sendCmd("G")    # start spinning
        self.waitWhileMoving()

        print "new home:",self.getPosition()

        self.zeroEncoder()


        # move to the first position (and brake will be set)
        f = self.defaults['FilterReverseLookup'][0]
        self.setPosition(self.defaults['FilterPositions'][f], slop=0)
        self.waitWhileMoving()
        

    def testRepro(self, n=1):
        """
        """
        fid = open("home.test","w")
        self.home()
        self.zeroEncoder()
        for i in range(n):
            c = self.getPosition()
            print >>fid, c,
            print c
            self.zeroEncoder()
            self.setPosition(-20000)
            #self.setPosition(-1000)
            self.go()
            print >>fid, self.getPosition()
            fid.flush() 
            self.home()

        c = self.getPosition()
        print >>fid, c,
        print c
        fid.close()

    def testHall(self, n=1):
        """
        """
        fid = open("hall.test","w")
        self.home()
        self.zeroEncoder()
        for i in range(n):
            self.home()
            print >>fid, self.getPosition()
            fid.flush()
        fid.close()

    def getPosition(self):
        """REad the current position"""
        p = int(self.term.sendCmd("RP",True, intType=True))
        return p

    def getAngle(self):
        """return the current position as an angle"""
        p = int(self.term.sendCmd("RP",True, intType=True))
        return self.countsToDeg(p)

    def go(self):
        """Update the motor."""
        print self.params
        # dont stop for the home switch
        self.term.sendCmd("UDO")
        # set mode
        self.term.sendCmd(self.params["mode"])
        # set velocity
        self.term.sendCmd("V=%i"%(self.params["velocity"]))
        print "velocity:",self.term.sendCmd("RV", True)
        # set acceleartion
        self.term.sendCmd("A=%i"%(self.params["acceleration"]))
        print "accel:",self.term.sendCmd("RA", True)

        # set position
        self.term.sendCmd("P=%i"%(self.params["position"]))
        #self.releaseBrake()
        self.term.sendCmd("G")
        self.waitWhileMoving()

        current = self.getPosition()
        self.params["position"]=current




_inst = None
def animatics(config=None):
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _animatics(config)
    return _inst


class timeOut(Exception):
    pass



def test():
    a = animatics()

    print "firmware:",a.readFirmware()
    
    #print a.term.sendCmd("RP",True)
    #print a.term.sendCmd("RA",True)

    #a.setPosition(10)
    
          

if __name__ == "__main__":
    test()
