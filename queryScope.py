

import sys, os, time
from omniORB import CORBA
import OtisCORBA
import Otis
import math


def airmass(z):
    """Estimate the airmass from the zenith angle."""
    #return 1./math.cos(z*math.pi/180)
    s = 1.0/math.cos(z*math.pi/180) - 1.0
    am = 1.0 + s*(0.9981833 - s*(0.002875
                            + s*(0.0008083)))
    return am

    

class _tele:
    """ """
    status = {
          'RA': 999,
	      'DEC': 999,
          'ALT': 999,
          'AZ': 999,
	      'POSANGLE': 999,
	      'AIRMASS': 1.0,
	      'RADECSYS': 'J2000',
	      'error': True,
	      }

    def __init__(self):
        self.status['error'] = self.connect()

    def connect(self):
        try:
            self.tele = Otis.getServer("Telescope")
        except Otis.NoServer:
            return True
        return False
    
    def queryTelescope(self, loop = 0):
        """ """
        if loop > 1:
            self.status['error'] = True
            return self.status
        try:
            t = self.tele.getStatus()
        except:
            self.status['error'] = self.connect()
            return self.queryTelescope(loop=loop+1)
        self.status['ALT'] = t.act_alt
        self.status['AZ'] = t.act_az
        self.status['POSANGLE'] = t.act_pa
        self.status['RA'] = t.act_ra
        self.status['DEC'] = t.act_dec
        self.status['AIRMASS'] = airmass(90-t.act_alt)
        return self.status
	

#------------------------------------ fake a singleton class
_inst = None
def telescope():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _tele()
    return _inst
#------------------------------------------------------------




def test():
    t = telescope()
    print t.queryTelescope()

if __name__=="__main__":
    test()
    
	
