#!/usr/local/bin/python

"""Filter wheel commands.


notes for ben:
you commented out duplicate code to sync position... if it works, delete it

you decided that the abort command should run immediately, not in a thread.
so clean up that code.
"""
import sys, time
import threader, threading
import OtisCORBA
import Log
from SkyProbeConf import config
import Utilities
import constants as const

if config['SimSkyProbe']:
    import animaticsSim as animatics
else:
    import animatics

class _FilterWheel:
    """
    """
    # configuration
    defaults = {'FilterNum': 5,
                'FilterLookup': {'g':0,'r':1,'i':2,'z':3,'y':4},
                'FilterReverseLookup': ["g", "r", "i", "z", "y"],
                'FilterPositions': {"g":0, "r":3818, "i":7680, "z":11523, "y":15350},
                
                'FilterCodes': {'NONE': '', 'SAME': 'same', 'DARK': 'dark'}
                }

    # status
    status = {'status': const.HOME_ME,
              'filterHomed': False,
              'filterCounts': 0,
              'filterSlot': 0,
              'filterName': 'g',
              'filterAngle': 0.0}

    
    # Create a lock for the focus-- this is set internally.
    intLock = threading.Lock()
    # this lock is set via the lock and unlock methods and is meant
    # to be controlled externally
    globalLock = threading.Lock()
    
    class setFilterCmd(threader.Command):
        """ A command class for threader to queue up.
        Set the filter position.
        """
        def __init__(self, parent, n, monitor):
            self.n = n
            self.monitor = monitor
            self.parent = parent
        def execute(self):
            try:
                self.parent._setFilter(self.n, self.monitor)
            except:
                self.error("Exception caught while changing the filter")
                raise

    class setFilterAngleCmd(threader.Command):
        """ Set the filter wheel position with an angle 0-360
        """
        def __init__(self, parent, deg, monitor):
            self.deg = deg
            self.monitor = monitor
            self.parent = parent
        def execute(self):
            try:
                self.parent._setFilterAngle(self.deg, self.monitor)
            except:
                self.error("Exception caught while setting filter angle")

    class homeFilterCmd(threader.Command):
        """ A command class for threader to queue up.
            Zero the focus ring.
        """
        def __init__(self, parent, monitor):
            self.parent = parent
            self.monitor = monitor
        def execute(self):
            try:
                self.parent._homeFilter(self.monitor)
            except:
                self.error("Exception caught while homing the filter")

    class updateStatusCmd(threader.Command):
        """ A thread to update the position status every so often.
        I've learned that we can't poll on every getstatus request when
        a bazilion clients are connected.
        """
        def __init__(self, parent, waitTime):
            self.parent = parent
            self.waitTime = waitTime
        def execute(self):
            try:
                self.parent._updateStatusLoop(self.waitTime)
            except:
                self.error("Exception caught in focus position polling loop")

    class abortFilterCmd(threader.Command):
        """ A command class for threader to queue up.
        """
        def __init__(self, parent):
            self.parent = parent
                    
        def execute(self):
            try:

                self.parent._abortFilter()
            except:
                self.error("Exception caught while aborting filter move")     
    
    def __init__(self):
        """
        """
        global config
        if config: Utilities.loadConfig(self.defaults, config)

        # Get the motor
        self.motor = animatics.animatics(config)

        self.status['status'] = const.READY

        # self.updatePosition()

        self.abortEvent = threading.Event()
        self.abortEvent.clear()

        # start the thread pool
        self.FilterThreadPool = threader.threadPool(5)
        
        # start motor polling thread
        cmd = self.updateStatusCmd(self, config['FilterPollTime'])
        self.FilterThreadPool.do(cmd, block=False)

    def lock(self):
        r = self.globalLock.acquire(False)
        if r: Log.entry("set global filter wheel lock")
        return r

    def unlock(self):
        Log.entry("Reset global filter wheel lock")
        self.globalLock.release()

    def validateFilter(self, f):
        """ Validate a filter string.  Empty strings are ok.
        """
        if f in self.defaults['FilterCodes'].values(): return True

        try:
            n = self.defaults['FilterLookup'][f]
        except KeyError:
            return False
        return True

    def shutdown(self):
        """
        """
        self.abortEvent.set()
        self.motor.shutdown()
        self.FilterThreadPool.shutdown()

    def _setFilter(self, n, monitor):
        """
        """
        self.status['status'] = const.BUSY
        self.intLock.acquire()
        #-------------------- 
        try:
            self.motor.setPosition(n)
        except:
            raise
        #--------------------
        #self.status['filterAngle'] = self.motor.getAngle()
        #self.status['filterCounts'] = self.motor.getPosition()
        #self.status['filterSlot'] = int(self.status['filterAngle'] / 72. + 0.5) % 5
        #self.status['filterName'] = self.defaults['FilterReverseLookup'][self.status['filterSlot']]
        #self.updatePosition()
       
        Log.entry("Filter changed to:"+str(n))
        self.status['status'] = const.READY
        self.intLock.release()
        if monitor: monitor.setDone()

    def _setFilterAngle(self, deg, monitor):
        """
        """
        self.status['status'] = const.BUSY
        self.intLock.acquire()
        self.angle = deg
        #-------------------
        try:
            self.motor.setAngle(deg)
        except:
            raise
        #-------------------
        #self.status['filterAngle'] = self.motor.getAngle()
        #self.status['filterCounts'] = self.motor.getPosition()
        #self.status['filterSlot'] = int(self.status['filterAngle'] / 72. + 0.5) % 5
        #self.status['filterName'] = self.defaults['FilterReverseLookup'][self.status['filterSlot']]

        self.updatePosition()  #does the above commands
        
        Log.entry("Set filter wheel angle to:"+str(deg))
        self.status['status'] = const.READY
        self.intLock.release()
        if monitor: monitor.setDone()

    def _homeFilter(self, monitor):
        """
        """
        self.status['status'] = const.HOMING
        self.intLock.acquire()
        #----------------------------- 
        try:
            self.motor.home()
        except:
            raise
        #-----------------------------

        #self.status['filterAngle'] = self.motor.getAngle()
        #self.status['filterCounts'] = self.motor.getPosition()
        #self.status['filterSlot'] = int(self.status['filterAngle'] / 72. + 0.5) % 5

        #self.updatePosition()
        
        Log.entry("Filter wheel homed.")
        self.status['filterHomed'] = True
        self.status['status'] = const.READY
        self.intLock.release()
        if monitor: monitor.setDone()

    def _abortFilter(self):
        """
        """
        self.motor.stop()
        Log.entry("Filter wheel move has been aborted!")

    def setFilter(self, string, monitor=None, block = True):
        """ Set the filter to n.  This command will be handled by a thread.
        """
        # if the filter wheel is not homed, just do it.
        if not self.status['filterHomed']:
            Log.entry("Homing the filter wheel")
            self.homeFilter()
        
        # check that we recognize the filter string
        if not self.validateFilter(string):
            if monitor: monitor.setError("Unknown filter name: "+string)
            raise UnknownFilterException

        try:
            n = self.defaults['FilterPositions'][string]
        except KeyError:
            # if the string is valid, but not a filter identifier,
            # don't change the wheel position.
            n = self.status['filterCounts']
            
        cmd = self.setFilterCmd(self,n, monitor)
        self.FilterThreadPool.do(cmd, block)
        return cmd
    
    def setFilterAngle(self, deg, monitor = None, block = True):
        """
        """
        # if the filter wheel is not homed, just do it.
        if not self.status['filterHomed']:
            Log.entry("Homing the filter wheel")
            self.homeFilter()
        
        #if deg < 0 or deg >= 360:
        #    if monitor: monitor.setError("Filter position out of range 0-360: "+str(deg))
        #    raise FilterWheelRangeException
        
        cmd = self.setFilterAngleCmd(self, deg, monitor)
        self.FilterThreadPool.do(cmd, block)
        return cmd

    def homeFilter(self, monitor=None, block = True):
        """ Home the filter.  This will be performed by a thread.
        """
        cmd = self.homeFilterCmd(self, monitor)
        self.FilterThreadPool.do(cmd, block)
        return cmd
            
    def abortFilter(self):
        """
        """
      
        #self.FilterThreadPool.do(self.abortFilterCmd(self))
        self._abortFilter()
        
        return 

    def updatePosition(self):
        """
        """
        self.status['filterAngle'] = self.motor.getAngle()
        self.status['filterCounts'] = self.motor.getPosition()
        self.status['filterSlot'] = int(self.status['filterAngle'] / 72. + 0.5) % 5
        self.status['filterName'] = self.defaults['FilterReverseLookup'][self.status['filterSlot']]

        

    def _updateStatusLoop(self, waitTime):
        """ Poll the motor forever or until the abort event is raised. """
        while not self.abortEvent.isSet():
            self.updatePosition()
            time.sleep(waitTime)
            

            
    def getStatus(self):
        """ return the busy status
        """
        return [self.status['status'], self.status['filterName'], self.status['filterSlot'], self.status['filterAngle'], self.status['filterHomed']]

###################################################
# Errors 
###################################################
    
class Error(Exception):
    pass
class UnknownFilterException(Error):
    pass
class FilterWheelRangeException(Error):
    pass






###################################################
# public
###################################################
#------------------------------------ fake a singleton class
_inst = None
def FilterWheel():
    """Fake a singleton class
    """
    global _inst
    if not _inst:
        _inst = _FilterWheel()
    return _inst
#------------------------------------------------------------





###################################################
# self test 
###################################################

def test():
    """ self test
    """
    filter = FilterWheel()
    #filter.homeFilter()
    #filter.setFilter("r")
    print "sleeping for 5 sec"
    time.sleep(10)
    

    #filter.abortFilter()

    filter.shutdown()

if __name__ == "__main__":
    """
    """
    test()  
