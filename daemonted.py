"""
Start a daemon process
Adapted from recipe: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/278731
"""

import os, signal, fcntl, time, sys


def createDaemon(config):
    """
    Detach a process from the controlling terminal and run it in the
    background as a daemon.  
    """
    print "i'm turning into a daemon"

    # read configuration parameters
    try:
        homeDir = config['HomeDir']
        stdoutFile = config['StdOutFile']
        stderrFile = config['StdErrFile']
    except KeyError:
        print >>sys.stderr, "It seems that you dont have HomeDir, StdOutFile and StdErrFile defined in config"
        return -1
        
    
    try:
        pid = os.fork()
    except OSError, e:
        return((e.errno, e.strerror))
      
    # kill my parent
    if (pid > 0): os._exit(0)
    
    # call setsid() to become the session leader
    os.setsid()

    # ignore SIGHUPs.  The grandchild will receive
    # one when its parent dies.
    signal.signal(signal.SIGHUP, signal.SIG_IGN)


    try:
        pid = os.fork()        # Fork a grandchild
    except OSError, e:
        return((e.errno, e.strerror))
    
    if (pid > 0): os._exit(0)  # kill the first child

    # change to our home directory
    os.chdir(homeDir)
    
    # let me go crazy with permissions
    os.umask(0)

    
  
    # Now redirect std output  to log files
    # this is a bit tricky...
    #
    print "look in the log files for stdout and stderr"
    print " stdout:",stdoutFile
    print " stderr:",stderrFile

    
    # first we close ALL possible file descriptors
    # these go from 1 to SC_OPEN_MAX
    try:
        maxfd = os.sysconf("SC_OPEN_MAX")
    except (AttributeError, ValueError):
        maxfd = 256       # default maximum
    
    for fd in range(0, maxfd):
        try:
            os.close(fd)
        except OSError:   # ERROR (ignore)
            pass
    # reopen stdin,stdout and stderr but redirect them

    # Redirect the standard file descriptors to a log file
    fd0 = os.open("/dev/null", os.O_RDONLY)                       # stdin  (0)
    if fd0 != 0:
        os.close(fd0)
        return -1
    
    fd1 = os.open(stdoutFile, os.O_CREAT|os.O_APPEND|os.O_WRONLY) # stdout (1)
    if fd1 != 1:
        os.close(fd0)
        return -2
    
    fd2 = os.open(stderrFile, os.O_CREAT|os.O_APPEND|os.O_WRONLY) # stderr (2)
    if fd2 != 2:
        os.close(fd0)
        os.close(fd1)
        return -3
    
    print "Skyprobe is a daemon"
    # We should have a living daemon now!
    return 0

