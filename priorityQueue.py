"""A simple priority queue.
"""

class action:
	"""A node for the priority queue.
	   Define an action with a priority rank and a duration.
	"""
	contents = None
	priority = None
	time = None

	def __init__(self, object, priority, time):
		"""
		"""
		self.contents = object
		self.priority = priority
		self.time = time

	def __str__(self):
		"""
		"""
		return str(self.contents)+", "+str(self.priority)+", "+str(self.time)


class priorityQueue:
	"""A simple priority queue.  High priority numbers == returned first.
	   Behavior for equal priority objects is last in, first out.
	"""

	queue = []

	def __init__(self):
		"""
		"""

	def enqueue(self, object, priority, time):
		"""
		Queue acts as LIFO
		"""
		newNode = node(object, priority, time)
		l = len(self.queue)

		if l == 0:
			self.queue.append(newNode)
			return

		if priority >= self.queue[l - 1].priority:
			self.queue.append(newNode)
			return

		if priority < self.queue[0].priority:
			self.queue.insert(0, newNode)	
			return

		i = 0
		while priority >= self.queue[i].priority:
			i+=1
		self.queue.insert(i, newNode)
		return

	def dequeue(self, timeAvail = None):
		"""dequeue
		   Optionally, input the available time.  Will return the
		   highest priority action that can be completed in time.
		   If no such action exists, return None.
		"""
		if len(self.queue) == 0:
			return None

		node = self.queue.pop()
		if timeAvail == None:  return node.contents

		tempQueue = priorityQueue()
		while node.time > timeAvail:
			tempQueue.enqueue(node.contents, node.

	def __str__(self):
		"""
		"""
		s = ""
		for i in range(len(self.queue)):
			s += str(self.queue[i])
			s += "\n"
		return s

def test():
	"""
	"""
	print "testing priorityqueue"
	q = priorityQueue()
	q.enqueue("a",1)
	q.enqueue("b",2)
	q.enqueue("c",3)
	q.enqueue("d",2)
	q.enqueue("e",2)
	q.enqueue("f",3)
	q.enqueue("g",1)
	print q.dequeue()
	print q.dequeue()
	print q.dequeue()
	print q.dequeue()
	print q.dequeue()
        print q.dequeue()
	print q.dequeue()



if __name__ == "__main__":
	test()
