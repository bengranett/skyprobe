#!/usr/local/bin/python
"""
a python class to communicate with a corba server.
Uses fnorb now

Ben Granett
"""

import sys, os, time


from omniORB import CORBA
import OtisCORBA


class __Otis:
    """
    A class to manipulate a corba server.  Don't use this class
    directly, but use getServer etc defined after.
    I put a __ prefix to disguise this class and make it pseudo private.
    """
    ##############################################
    # hard wire the server name and port...
    configHost = "flaxen"
    configPort = 1050
    ##############################################
    
    def __init__(self):
        """
        """
        self.orb = CORBA.ORB_init()
        self.servers={}
    
    def getConfigIOR(self):
        """
        Return the IOR string of the Config server.
        """
        import socket
        iorString = ""
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect((self.configHost,self.configPort))
            iorString = sock.recv(1024)
            sock.close()
        except socket.gaierror:
            print >>sys.stderr, "Cannot connect to server",self.configHost, self.configPort
        except socket.error:
            print >>sys.stderr, "Connection refused at",self.configHost, self.configPort
    
        return iorString

    
    def getIOR(self, serverName):
        """
        retrieve the IOR string for a given host string
        """
        serverIOR = self.getServer("Config").get("IOR/"+serverName)
        
        if serverIOR == "":
            print >>sys.stderr, "host "+serverName+" not listed on config server"
            
            
        return serverIOR
        
    def registerIOR(self, serverName, iorString):
        """
        register a server
        """
        config = self.getServer("Config")
        config.put("IOR/"+serverName, iorString)
        
        
    def removeIOR(self, serverName):
        """
        remove a server
        """
        config = self.getServer("Config")
        config.remove("IOR/"+serverName)
        
        

    def getServer(self, serverName, timeout=0):
        """
        Attempt to connect to a server
        """
        # set retry interval to 1 second
        retryInterval = 1
        if timeout == 0: retryInterval = 0
        startTime = time.time()
        
        count = 0
        while(time.time() - startTime < timeout or count < 1):
            count += 1
            # check if we already have this server
            if not self.servers.has_key(serverName):
                iorString = ""
                if serverName == "Config":
                    iorString = self.getConfigIOR()
                else:
                    config = self.getServer("Config")
                    iorString = self.getIOR(serverName)
                    
                if iorString == "":
                    time.sleep(retryInterval)
                    continue
                
                # save the server
                self.servers[serverName] = self.orb.string_to_object(iorString)
            
            server = self.servers[serverName]
            
            # check that this server is alive
            alive = 0
            try:
                if not server.isAlive():
                    print >>sys.stderr, serverName+".isAlive() returns false"
                    sys.exit(1)
                alive = 1
            # catch exceptions
            except CORBA.TRANSIENT:
                ############################################
                # didn't work, but it's worth another shot
                ###########################################
                print >>sys.stderr, "caught corba.transient exception"
                time.sleep(retryInterval)
            except CORBA.COMM_FAILURE:
                ######################################
                # The server is probably down
                # but we'll wait and see if someone
                # brings it back up for us
                ######################################
                print >>sys.stderr, "caught CORBA comm failure exception"
                time.sleep(retryInterval)
                # force us to fetch again
                
                del self.servers[serverName]
            except CORBA.NO_IMPLEMENT:
                ############################################
                # it's prolly OK to exit on this one too
                ############################################
                print >>sys.stderr, "no implementation"
                sys.exit(1)
            except:
                print >>sys.stderr, "Unknown exception caught while trying",serverName
                raise
                sys.exit(1)
                
            # the server is working!
            if alive:
                return server
                
        #print >>sys.stderr, "Could not get server", serverName
        raise NoServer(serverName, count)

		
class Error(Exception):
        """
        """
        pass

class NoServer(Error):
        """
        """
        def __init__(self, name, attempts):
                """
                """
                self.attempts = attempts
                print >>sys.stderr, "Could not get server",name,"after",attempts,"attempts."

	
		
# Create an instance of __Otis.
# this be a singleton!
__OtisCom = __Otis()

###############################################
# To make things easy, here are the public
# functions.  I think everything else should
# be considered private.
###############################################
def getServer(host, timeout = 0):
	"""
	Connect to the server and return the instance
	"""
	return __OtisCom.getServer(host, timeout)
	
def registerIOR(serverName, iorString):
	"""
	Register a server name/IOR string pair
	"""
	__OtisCom.registerIOR(serverName, iorString)

def removeIOR(serverName):
	"""
	Remove a server name/IOR from the database
	"""
	__OtisCom.removeIOR(serverName)
	
##################################################
# done with public defs
##################################################


def test():
    print "...testing..."
    
if __name__ == "__main__":
    test()
