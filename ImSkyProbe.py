""" SkyProbe class

This class defines the CORBA commands.


Notes:
implement halt command yet

Need to implement powerdown, which is supposed to turn off the computer completely.
"""

import globals, Focus, FilterWheel, Camera, Sequencer, getStatus, Thermometer
from SkyProbeConf import config

from OtisTypes import *

###############################
# Use omniorb
from omniORB import CORBA, PortableServer
import OtisCORBA
import OtisCORBA__POA
###############################

import Log
import Heartbeat
import SPMonitor

verb = True

class SPServer(OtisCORBA__POA.SkyProbe):
    """
    """
    ALIVE = 1
    myName =  "ImSkyProbe"
    
    def __init__(self, orb):
        """ pass the BOA in so we can kill our self.
        """
        # set configuration values
        try:
            self.myName = config['MYNAME']
        except:
            pass
        
        self.orb = orb
        
        # instantiate the components
        self.sequencer = Sequencer.Sequencer()
        self.focus = Focus.Focus()
        self.filterWheel = FilterWheel.FilterWheel()
        self.camera = Camera.Camera()
        self.thermom = Thermometer.Thermometer()
        
        
    ###############################
    # Generic server routines
    ###############################
    def getName(self):
        """
        return the server name
        """
        return self.myName

    def isAlive(self):
        """
        Am I alive?
        """
        return self.ALIVE
        
    def shutdown(self, code = 0):
        """
        shutdown the server
        """
        if verb: Log.entry("Received shutdown command", 2)

        globals.shutdown()

        # 0 return immediately
        # 1 block until processing is complete, then return
        # want to use 0 so the server can be shutdown remotely :)
        # use 1 if you are shutting down locally with a ^C so it cleans up
        self.orb.shutdown(code)
        
        
    ###############################
    # SkyProbe specific routines
    ###############################

#    def getReady(self, type, exposure, id, ra, dec, track, stars):
#        """ get ready for an exposure sequence
#        * This informs the sky probe that the telescope will
#        * be pointing at a particular target for a given period
#        * of time. The sky probe is then free to decide what
#        * to do during that time. Typically it will perform
#        * a series of observations using different filters
#        * The parameters are as follows:
#        * type - is the telescope pointing at the sky or a flat?
#        * exposure - the minimum time the telescope will be
#        *            pointing at the target.
#        * id - the unique ID associated with this pointing
#        * ra, dec - the commanded boresight position on the sky
#        * track - the commanded track in Az/Alt that the telescope
#        *         will follow.
#        * stars - a list of stars which may be visible to the sky probe
#        """
#        # set parameters
#        # move filter wheel if necessary
#        # go back to sleep
#        pass

        
    def abort(self):
        """
        Command the sky probe to interrupt a sequence of exposures.
        if the sky probe is not in a sequece this command does nothing.
        This will typically be called just before the telescope
        slews to a new target.
        """
        if verb: Log.error("Received abort command.  But not yet implemented.", 2)
        #skyprobe.abort()
        

    def powerDown(self):
        """
        SkyProbe should shutdown cleanly and power down.  Call in case of
        unexpected loss of power, or for routine shutdown.
        """
        if verb: Log.entry("Received powerDown command.  This is not yet fully implemented.  The server will shutdown but the computer will not power off.", 2)

        self.shutdown()

    def useAutoMode(self, avail_time):
        """
        Indicates that the sky probe should ignore previously set sequences
        and use its best judgement to choose an exposure sequence which
        will fit into the given available time in seconds.
        This sequence will be executed by subsequent "go" commands.
        "Auto mode" is canceled the next time someone calls setSequence.
        """
        Log.entry("useAutoMode called.  Sorry, not yet implemented!")

    def setSequence(self, seq):
        """
        Sets the sequence of exposures (filters and exposure times)
        which will be taken after issuing the next "go" command
        Note the user is responsible for ensuring that the sequence
        can be executed in the time available.
        raises BadCommandException
        """
        self.sequencer.loadSeq(seq)    
    
    def setTargetType(self, type):
        """
        Indicates the type of data taken by subsequent exposures
        as either night sky, twilight flat, dome flat, or focus sequence.
        """
        self.sequencer.setTarget(type)

    def setReadoutMode(self, mode):
        """ Sets the readout mode.of the ccd
        """
        self.camera.setReadOutMode(mode)

    def setID(self, id):
        """ sets the full ID for the next exposure. Each exposure must
        have a unique ID. This value is internally cleared after each
        call to "go()". Ths method should only be used by automation
        which can be trusted to keep the IDs unique.
        """
        try:
            print "going to set the id to",id
            self.sequencer.setID(id)
        except:
            raise
            raise OtisCORBA.BadCommandException

    def setIDBase(self, idbase):
        """ Specify a string to appear in the unique ID for each
        subsequent exposure. The server will then assign the unique IDs
        usually by appending a sequential count to the given string.
        This method should be used by manual control systems where
        a human operator is choosing the ID. The current full unique
        ID is given in the SkyProbeStatus struct.
        """
        try:
            self.sequencer.setIDBase(idbase)
        except:
            raise OtisCORBA.BadCommandException("")
    
    def go(self, monitor):
        """
        Initiate a sequence of exposures.
        "id" is a unique string identifying the set of exposures.
        This string should be used in the names of the files generated
        from these exposures. If there is a corresponding Camera (GPC)
        exposure, it will be identified by the same string.
        This command should return immediately, and notify the
        monitor asynchronously when the sequence is done
        """
        myMonitor = SPMonitor.MonitorMan(monitor)
        myMonitor.setRegistered()
        try:
            self.sequencer.go(myMonitor)
        except Sequencer.IDExpiredError, error:
            myMonitor.setError(str(error))
            raise OtisCORBA.BadCommandException(str(error))

    def setFilterAngle(self, degrees, monitor):
        """
        Sets an arbitrary position of the filter wheel.
        This command should return immediately and notify the monitor
        when the filter wheel has arrived at the commanded position 
        """
        myMonitor = SPMonitor.MonitorMan(monitor)
        myMonitor.setRegistered()
        if not self.filterWheel.lock():
            myMonitor.setError("Filter wheel in use.")
            return
        myMonitor.addUnLock(self.filterWheel.unlock)
        try:
            self.filterWheel.setFilterAngle(degrees, myMonitor, block=False)
        except FilterWheel.FilterWheelRangeException:
            raise OtisCORBA.BadCommandException("")
        
    def setFilter(self, filter, monitor):
        """
        Command the filter wheel to put the named filter into the
        beam. Valid strings are "g", "r", "i", "z" and "y"
        """
        myMonitor = SPMonitor.MonitorMan(monitor)
        myMonitor.setRegistered()
        if not self.filterWheel.lock():
            myMonitor.setError("Filter wheel in use.")
            return
        myMonitor.addUnLock(self.filterWheel.unlock)
        try:
            self.filterWheel.setFilter(filter, myMonitor, block=False)
        except FilterWheel.UnknownFilterException:
            raise OtisCORBA.BadCommandException("")
        
    def homeFilter(self, monitor):
        """
        Causes the filter wheel to "home" its encoders
        """
        myMonitor = SPMonitor.MonitorMan(monitor)
        myMonitor.setRegistered()
        if not self.filterWheel.lock():
            myMonitor.setError("Filter wheel in use.")
            return
        myMonitor.addUnLock(self.filterWheel.unlock)
        try:
            self.filterWheel.homeFilter(myMonitor, block=False)
        except:
            raise OtisCORBA.BadCommandException("")
            
    def setFocus(self, n, monitor):
        """
        set the focus to a particular value
        """
        myMonitor = SPMonitor.MonitorMan(monitor)
        myMonitor.setRegistered()
        if not self.focus.lock():
            myMonitor.setError("Focus in use.")
            return
        myMonitor.addUnLock(self.focus.unlock)
        try:
            self.focus.setFocus(n, myMonitor, block=False)
        except OtisCORBA.BadCommandException:
            raise OtisCORBA.BadCommandException("")
        
    def homeFocus(self, monitor):
        """
        Homes the focus motor.
        """
        print "1"
        
        myMonitor = SPMonitor.MonitorMan(monitor)
        print "2"

        myMonitor.setRegistered()

        print "3"
        if not self.focus.lock():
            myMonitor.setError("Focus in use.")
            return
        myMonitor.addUnLock(self.focus.unlock)
        try:
            self.focus.homeFocus(myMonitor, block = False)
        except OtisCORBA.BadCommandException(""):
            raise


    def setCCDCooler(self, status):
        """
        Set the state of the CCD cooler.  status is boolean on (True) or off (False).
        """
        self.camera.setCCDCooler(status)

    def setCCDThermostat(self, celcius):
        """
        Set the temperature goal for the CCD
        """
        self.camera.setCCDThermostat(celcius)


    def halt(self):
        """
        stop all motion inside the skyprobe. The sky probe should
        then not be able to function until reset() is called.
        This is an emergency stop.
        """
        pass

    def reset(self):
        """restore the skyprobe to functioning status after a call to halt().
        """
        pass
        
    def getStatus(self):
        """
        Query the current status of the sky probe
        """
        return getStatus.getStatus() 


def init():
    """ initialize something
    """
    print "init"
    globals.init()
    print "init done"
