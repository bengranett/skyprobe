class Date:
    """
    an instance in time
    """
    # the number of seconds since 1970-01-01T00:00:00
    # excluding leap seconds.
    seconds = None

    # the number of nanoseconds since the last
    # integral second
    nanosec = None

    # true if the current time occurs during a leap second
    leapsec = None


class TrackPoint:
    """
    Specifies the path the telescope follows across the sky.
    interpolation should be done using a cubic spline.
    """
    time = Date()
    az = None
    alt = None
    rotator = None

class Star:
    """
    Specifies the elements in the star catalog
    """
    name = ""
    ra = None
    dec = None
    mag = None

    segment = ""
    x = None
    y = None

class Exposure:
    """
    """
    def __init__(self, filter=None, focus = -1, exposure = 0):
        self.filter = filter 
        self.exposure = exposure 
        self.focus = focus 
    def __str__(self):
        return str([self.filter, self.focus, self.exposure])

class SkyProbeStatus:
	"""
	"""
	focus = None
	filter = None
	filter_position = None
	seq = None
	target_type = None
	auto_mode = None
	state = None
	exposure = None
	elapsed = None

