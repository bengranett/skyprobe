#!/usr/local/bin/python

"""
priorityQueue.py: priority queue
"""


class Node:
	"""
	Define a node for a binary tree.
	"""
	left = None
	right = None

	priority = None
	contents = None

	def __init__(self, contents, priority, left, right):
		"""
		"""
		self.conents = contents
		self.priority = priority
		self.left = left
		self.right = right	



class Queue:
    """
    A priority queue, based on a heap.
    """

    def __init__(self):
        """
        """
        queue = None


    def enqueue(self, something, priority = 0):
        """
        enqueue something.  Organize by priority
        """
        zip = (something, priority)
        
        if priority > queue[0]:
            queue.insert(0, zip)
            return
        if priority <= queue[len(queue)-1]:
            queue.append(zip)
            return
        

    def dequeue(self):
        """
        dequeue next item
        """
        
