""" Sky Probe startup and shutdown routines
"""



import threader, Log
import Focus, FilterWheel, Sequencer, Camera
import Heartbeat
import Thermometer

def init():
    """ initialize the global therad pool and other things.
    """
    global focus, filterWheel, camera, sequencer, thermo
    
    focus = Focus.Focus()
    filterWheel = FilterWheel.FilterWheel()
    sequencer = Sequencer.Sequencer()
    camera = Camera.Camera()
    thermo = Thermometer.Thermometer()

    Heartbeat.start()
    Log.entry("mysql heartbeat started")

def shutdown():
    """ Kill all the components.  This is important or else threads will hang.
    """
    camera.shutdown()
    Heartbeat.shutdown()
    focus.shutdown()
    filterWheel.shutdown()
    sequencer.shutdown()
    thermo.shutdown()
