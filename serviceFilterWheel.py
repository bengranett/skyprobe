import sys
import animatics as ani
from SkyProbeConf import config

if len(sys.argv) == 1:
    print "Usage: %s command"%(sys.argv[0])
    sys.exit(0)

com = sys.argv[1]


a = ani.animatics()

try:
    assert(a.readFirmware().strip() == "24576/415C")
except:
    print "wrong firmware!!  motor probably not connected..."
    sys.exit()

if com == "off":
    a.releaseBrake()
    print "brake is released"
    #a.powerOff()
    #print "power is off"
elif com== "go":
    pos = sys.argv[2]

    dict = config['FilterPositions']
    try:
        n = dict[pos]
    except keyError:
        print "filter position must be one of:"
        print dict.keys()
        sys.exit(0)
    a.setPosition(n)
elif com == "home":
    a.home()
elif com == "halfway":
    dict = config['FilterPositions']
    g = dict['g']
    r = dict['r']
    a.setPosition((g+r)/2)
elif com == "status":
    print "**** Animatics motor status ****"
    print "Firmware:", a.readFirmware()
    print "Position:", a.getPosition()
    print "Acceler: ", a.term.sendCmd("RA", True)
    print "Velocity:", a.term.sendCmd("RV", True)
    print "Angle:   ", a.getAngle()
    print "moving?  ", a.isMoving()
    print "tempera: ", a.readTemperature()
    print "********************************"
