from OtisCORBA import SkyProbe as OtisTypes
import OtisCORBA
import isptime
import Sequencer, Focus, FilterWheel, Camera, Thermometer
from SkyProbeConf import config
import constants as const


def getStatus():
    """This routine is a mess
    """
    # populate the date structure
    sec,nanosec,leap = isptime.time()
    timeStamp = OtisCORBA.Date(sec, nanosec, leap)

    # grab instances of the hardware components to poll
    FW = FilterWheel.FilterWheel()
    Foc = Focus.Focus()
    seq = Sequencer.Sequencer()
    cam = Camera.Camera()
    thermo = Thermometer.Thermometer()

    # get other stats
    focusStat = Foc.getStatus()
    filterStat = FW.getStatus()
    seqStat = seq.getStatus()
    camStat = cam.getStatus()


    # make up the status structure
    # I dont know if you can do this without a bunch of Nones!!
    status = OtisTypes.SkyProbeStatus(None, None, None, None,
                                      None, None, None, None,
                                      None, None, None, None,
                                      None, None, None, None,
                                      None, None
                                      )

    # get the focus position
    status.focus = focusStat[1]
    status.focus_homed = focusStat[2]

    # get filter stats
    status.filter = filterStat[1]           # filter letter
    status.filter_position = filterStat[3]  # filter angle
    status.filter_homed = filterStat[4]     # filter homed status
    

    # get sequencer stats
    status.seq = seqStat['expSeq']
    status.target_type = seqStat['targetType']
    status.id = seqStat['seqID']
    if not status.id:
        status.id = "notset"
        

    status.auto_mode = 0

    # get camera stats
    camera = camStat['status']
    status.exposure = camStat['ExposureTime']
    status.elapsed = camStat['ElapsedTime']
    status.readout = camStat['readOutMode']
    status.cooler_on = camStat['CoolerOn']
    status.ccd_temperature = camStat['CCDTemp']
    status.cooler_set_point = camStat['CoolerSetPoint']
    status.cooler_drive = camStat['CoolerDrive']

    # load temperature measurements
    # check the temperature
    tempdict, lastRead = thermo.readTemp()
    status.temperatures = []
    for key in tempdict.keys():
        status.temperatures.append(OtisTypes.TemperatureProbe(key, tempdict[key]))

    # make up a color code
    color = OtisCORBA.YELLOW
    mesg = config['TextIdle']


   
    if camera == const.EXPOSING:
        color = OtisCORBA.GREEN
        mesg = config['TextExposing']

    if camera == const.READING:
        color = OtisCORBA.YELLOW
        mesg = config['TextReading']
    
    if focusStat[0]==const.CHANGING or filterStat[0]==const.CHANGING:
        color = OtisCORBA.YELLOW
        mesg = config['TextChanging']

    if focusStat[0]==const.HOMING or filterStat[0]==const.HOMING:
        color = OtisCORBA.YELLOW
        mesg = config['TextHoming']

    if focusStat[0]==const.HOME_ME:
        color = OtisCORBA.RED
        mesg = config['TextHomeFocus']
        
    if camera == const.ERROR:
        color = OtisCORBA.RED
        mesg = config['TextCameraError']
           
    indic = OtisCORBA.StateIndicator(mesg, color, timeStamp,["This is SkyProbe actual."])
    status.state = indic

    #printStatus(status)

    return status


def printStatus(status):
    """"""
    print "status:"
    print "------------------------"
    print status.state.details
    print "time:   ",status.state.time.seconds, status.state.time.nanosec, status.state.time.leapsec
    print "focus:  ",status.focus
    print "focus_homed:",status.focus_homed
    print "filter: ",status.filter
    print "angle:  ",status.filter_position
    print "filter homed:", status.filter_homed
    print "type:   ",status.target_type
    print "automof:",status.auto_mode
    print "duratio:",status.exposure
    print "elapsed:",status.elapsed
    print "status: ",status.state.text,"Color:", status.state.color
    for temp in status.temperatures:
        print temp.name, temp.celsius
    i=0
    print "seqid:",status.id
    print "Loaded sequence:"
    for exp in status.seq:
        i+=1
        print "  exp "+str(i)+":",exp.duration, exp.filter, exp.focus
    print "read out mode:", status.readout
    print "CCD temp:", status.ccd_temperature
    print "CCD Cooler:", status.cooler_on
    print "CCD setpoint:", status.cooler_set_point
    print "CCD cool drive:", status.cooler_drive
    print "------------------------"
