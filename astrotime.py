import time

def julianDate(t):
    """ Return the julian date given a time tuple
        tuple should be in the format returned by time.gmtime()

        Jan 1 2005 0:0:0 is julian date 2453371.5
        We'll use this as a reference, just don't
        expect accurate results before 1582
    """
    # jan 1 2005
    j0 = 2453371.5
    
    # find seconds between 1970 and jan 1 2005 
    sec_ref = time.mktime((2005, 1, 1, 0, 0, 0, 0, 1, 0))
       
    # find seconds between 1970 and the time of interest
    sec = time.mktime(t)

    # subtract and convert to days
    days = (sec - sec_ref)/3600./24.

    # sum and return easy!
    return j0 + days


def equinox(t):
    """ Return the equinox given the time tuple
        Assume year is 365 days.
        I don't know if you are supposed to correct for leap years.
        lets truncate to two sig figs
    """
    year = t[0]
    yday = t[7]

    string = "%4.2f" % (year + yday/365.)

    return float(string)



