#!/usr/local/bin/python

import pylab
import database
import time


tnow = time.time()
tstart = 1130187400


d = database.db(host="",user="",passwd="",db="")

labels = labels=[("time", tstart, None),"temperature1"]
    
data = d.select("sky_probe_hk",labels)


x = []
y = []
for d in data:
    x.append((d[0]-tstart)/3600.)
    y.append(d[1])
    #print d[0],d[1]


pylab.plot(x,y, linewidth=2)
pylab.xlabel("Hours since "+time.asctime(time.localtime(tstart)))
pylab.ylabel("Temperature (C)")
pylab.ylim(25,31)

pylab.show()
