""" Send the SkyProbe I heartbeat
"""

import threader,threading
import SkyProbeDB
import isptime
import random,sys
import Thermometer
import Camera
from SkyProbeConf import config
import constants as const

class heartbeatCmd(threader.Command):
    """
    """
    def __init__(self):
        """
        """
        self.abortEvent = threading.Event()
        self.abortEvent.clear()
        self.thermo = Thermometer.Thermometer()
        self.camera = Camera.Camera()

    def execute(self):
        """
        """
        self.startHeartbeat()

    def startHeartbeat(self):
        """
        """
        print "starting heartbeat"
        self.db = SkyProbeDB.ispdb()

	self.go()

    def shutdown(self):
        """
        """
        self.abortEvent.set()



    def fillTempTable(self, time, dict):
	"""
	"""
	for key in dict.keys():
	    values = (time, key, dict[key])
	    self.db.insert(config['MYSQLTemperatureTable'], config['MYSQLTempLabels'], values)

    def go(self):
        """
        """

        while not self.abortEvent.isSet():
	    seconds = isptime.time()[0]

            camStat = self.camera.getStatus()
            
	    isp_status = 0
	    values = (seconds, 
		      camStat['CoolerSetPoint'], 
		      camStat['CoolerDrive'],
		      camStat['CoolerStatus'], 
		      isp_status, 
		      camStat['status'])

            self.db.insert(config['MYSQLHKTable'], config['MYSQLHKLabels'], values)

	    # loop through all the temperatuer dictionaries
	    camTemp = camStat['temperatures']
	    camTempTime = int(camStat['LastRead'])
	    self.fillTempTable(camTempTime, camTemp)
	    
	    thermomTemp, thermomTempTime = self.thermo.readTemp()
	    self.fillTempTable(thermomTempTime, thermomTemp)

	    # add motor here


            # print values
            # wait for 30 seconds, but bail on abort event.
            self.abortEvent.wait(timeout=config['HeartBeatSleep'])
            
        
            


def start():
    global hb,pool
    
    pool = threader.threadPool(1)
    hb=heartbeatCmd()
    pool.do(hb)

def shutdown():
    global hb, pool
    hb.shutdown()
    pool.shutdown()
    


def test():
    start()
    print "heartbeat going"

    done = False
    while not done:
        print "q to quit!  >>",
        line = sys.stdin.readline()
        if line[0] == "q":
            done = True 
            break

    shutdown()
    print "heart beat shutdown"

    pool.shutdown() 

if __name__=="__main__":
    test()
