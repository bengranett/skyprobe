#!/usr/local/bin/python
"""
SkyProbe client

Ben Granett
"""

import sys, os
import ORBit, CORBA
import Otis



ORBit.load_file("OtisCORBA.idl")
import OtisCORBA

try:
    sp = Otis.getServer("SkyProbe", OtisCORBA.SkyProbe, 5)
except Otis.NoServer:
    print "server is not up"
    sys.exit(1)


if sp.isAlive():
    print "success!"
    
#print sp.getName()
#sp.abort()
    
#print "shutting server down"

# we always get a comm failure exception, so catch it
#try:
#    sp.shutdown()
#except CORBA.COMM_FAILURE:
#    print "lost connection"
