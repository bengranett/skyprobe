
import os

def findNumInString(name):
    """i, num, root = OSUtils.findNumInString(ext)
    """
    j=1
    while name[-j:].isdigit():
        j += 1
        if j > len(name): break
    print j

    j -= 1
    if j == 0:
        num = "0"
    else:
        num = name[-j:]
    print num
    i = len(name)-j
    root = name[0:i]
    return i, int(num), root

def findFileName(fileName, extension = ".fits"):
    """ get a unique filename
    """
    #print fileName

    # first make sure the filename has the desired extension
    try:
        n = fileName.rindex(".")
    except ValueError:
        # no dot in the filename...
        # then add a .fits ending
        fileName = fileName + extension
        # try again
        return findFileName(fileName)

    # now check if the file exists
    try:
        s = os.stat(fileName)
    except OSError:
        # file does not exist... good
        return fileName

    # if it exists, we want to increment a counter in the name
    root = fileName[0:n]
    suffix = fileName[n:]    # .fits
    
    # count the number of digits if there
    # is a number at the end of the filename
    n = 1
    while root[-n:].isdigit(): n+=1
    n-=1
    if n < 1: return findFileName(root+"1"+suffix)
    
    num = int(root[-n:])
    base = root[0:-n]
    
    num+=1
       
    return findFileName(base+str(num)+suffix)



def test():
    #print findFileName("test", ".fits")
    print findNumInString("hello")
    print findNumInString("hello45")
    print findNumInString("32432")

if __name__=="__main__":
    test()


