""" focus scripts
"""
import Focus
import globals
import threader


class focusSequenceCmd(threader.Command):
    """ perform a focus sequence.
    """
    def __init__(self, focusPoints, expTimes):
        """
        """
        self.focusPoints = focusPoints
        self.expTimes = expTimes
        
        
    def execute(self):
        """
        """
        try:
            _focusSequence(self.focusPoints, self.expTimes)
        except:
            raise
            

def _focusSequence(focusPoints, expTimes):
    print "starting focus sequence"
    for i in range(len(focusPoints)):
        Focus.setFocus(focusPoints[i])
        #snap(expTimes[i])
        #measureImage()
    #select best focus
    #Focus.setFocus(best)
    
    


def test():
    globals.init()
    Focus.init()
    Focus.zeroFocus(block=False)

    focusPoints=[2,4,5,6,7,8]
    expTimes = [1,1,1,1,1,1]
    job = focusSequenceCmd(focusPoints, expTimes)
    globals.threadPool.do(job, block=True)
    
    print "i'm focusing in the bg"

    globals.threadPool.shutdown()
    Focus.shutdown()



if __name__ == "__main__":
    test()
